//package si.uni_lj.fri.pbd2019.runsup;
//
//import android.support.test.espresso.Espresso;
//import android.support.test.espresso.assertion.ViewAssertions;
//import android.support.test.espresso.matcher.ViewMatchers;
//import android.support.test.filters.LargeTest;
//import android.support.test.rule.ActivityTestRule;
//import android.support.test.runner.AndroidJUnit4;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//
//@RunWith(AndroidJUnit4.class)
//@LargeTest
//public class WorkoutServiceTest {
//
//    @Rule
//    public ActivityTestRule<MainActivity> activityRule =
//            new ActivityTestRule<>(MainActivity.class);
//
//    @Test
//    public void listGoesOverTheFold() {
//        Espresso.onView(ViewMatchers.withText("Hello world!")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
//    }
//}


package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.DrawerMatchers;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.Gravity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import si.uni_lj.fri.pbd2019.runsup.MainActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class WorkoutServiceTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.ACCESS_FINE_LOCATION");

    @Test
    public void clickStartStopContinue() {
        Espresso.onView(allOf(ViewMatchers.withId(R.id.button_stopwatch_start), withText("Start"))).perform(click());
        try {
            Thread.sleep(1200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // stop workout
        Espresso.onView(allOf(withId(R.id.button_stopwatch_start), withText("Stop"), isEnabled())).perform(click());
        // continue workout
        Espresso.onView(allOf(withId(R.id.button_stopwatch_start), withText("Continue"))).perform(click());
        // check
        Espresso.onView(withId(R.id.button_stopwatch_start)).check(ViewAssertions.matches(withText("Stop")));
    }


    @Test
    public void tryToOpenWorkoutDetail() {

        DatabaseService helper = DatabaseService.getInstance(InstrumentationRegistry.getTargetContext());

        helper.deleteAllWorkouts2();

        Workout workout = helper.createWorkout(SportActivities.RUNNING, Workout.statusEnded);
        openHistoryFragment();

        Espresso.onData(anything()).inAdapterView(withId(R.id.listview_history_workouts)).atPosition(0).perform(click());
        intended(allOf(IntentMatchers.hasComponent("si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity"), IntentMatchers.hasExtra("workoutId", workout.getId())));
    }

    @Test
    public void endedWorkoutShouldBeShowInHistory() {
        DatabaseService helper = DatabaseService.getInstance(InstrumentationRegistry.getTargetContext());
        Workout workout = helper.createWorkout(SportActivities.RUNNING, Workout.statusEnded);

        openHistoryFragment();
        Espresso.onView(withText(workout.getTitle())).check(ViewAssertions.matches(isDisplayed()));
    }


    public void openHistoryFragment() {
        try {
            Thread.sleep(1200);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        Espresso.onView(withId(R.id.drawer_layout))
                .check(ViewAssertions.matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());

        Espresso.onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        ViewInteraction appCompatCheckedTextView = Espresso.onView(
                allOf(withId(R.id.design_menu_item_text), withText("History"), isDisplayed()));
        appCompatCheckedTextView.perform(click());
    }
}


