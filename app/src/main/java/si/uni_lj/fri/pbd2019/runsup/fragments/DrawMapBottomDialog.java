package si.uni_lj.fri.pbd2019.runsup.fragments;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.EventsOverview;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.pojo.Event;
import si.uni_lj.fri.pbd2019.runsup.services.DirectionsService;
import si.uni_lj.fri.pbd2019.runsup.services.FirestoreService;

public class DrawMapBottomDialog extends BottomSheetDialogFragment {

    private static String TAG = "WorkoutDetailMapFragment";
    private String accId;
    private String LINE;
    private Calendar calendar;
    private TextView tvDateStart;
    private Button buttonSetStartDate;
    private Button buttonConfirm;

    private DirectionsService directionsService;


    public DrawMapBottomDialog() {
    }

    public static DrawMapBottomDialog newInstance(String param1, String param2) {
        DrawMapBottomDialog fragment = new DrawMapBottomDialog();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        directionsService = DirectionsService.getInstance(getActivity());
        return inflater.inflate(R.layout.custom_bottom_sheet, viewGroup, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        accId = sharedPref.getString("accId", null);
        LINE = sharedPref.getString("LINE", null);

        Log.d(TAG, "onCreateView: accId= " + accId);
        Log.d(TAG, "onCreateView: LINE= " + LINE);

        tvDateStart = (TextView) view.findViewById(R.id.custom_button_sheet_textview_dateStart);
        buttonSetStartDate = (Button) view.findViewById(R.id.custom_button_sheet_set_date);
        buttonConfirm = (Button) view.findViewById(R.id.custom_button_sheet_button_confirm);


        if (tvDateStart != null) {
            buttonSetStartDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "1");
                    showDateTimePicker();
                    Log.d(TAG, "100");
                }
            });
        }
        if (buttonConfirm != null) {
            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addNewEvent();
                }
            });
        }

        Log.d(TAG, "tvDateStart= " + (tvDateStart != null ? "OK" : "NULL"));
        Log.d(TAG, "buttonConfirm= " + (buttonConfirm != null ? "OK" : "NULL"));
    }

    public void showDateTimePicker() {
        Log.d(TAG, "2");
        final Calendar currentDate = Calendar.getInstance();
        calendar = Calendar.getInstance();
        new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("", "year= " + year);
                calendar.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(getActivity(), R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        Log.v("", "The choosen one " + calendar.getTime());

                        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        Log.d("", "before format= " + calendar.getTime());
                        String dateStartFormated = dateFormat.format(calendar.getTime());
                        tvDateStart.setText(dateStartFormated);

                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void addNewEvent() {

        String title = "Title";
        Date dateStart = (calendar != null ? calendar.getTime() : null);

        Log.d(TAG, "addNewEvent1");
        List<LatLng> potionsList = directionsService.getPotionsList();
        Log.d(TAG, "addNewEvent2");

        Log.d("MOJ", "directionsService.getPositionListSize() >= 2: " + (directionsService.getPositionListSize() >= 2));
        Log.d("MOJ", "LINE= " + LINE);
        Log.d("MOJ", "accId= " + accId);

        if (accId == null) {
            Toast.makeText(getActivity(), "Log in to add new event", Toast.LENGTH_LONG).show();
        } else if (directionsService.getPositionListSize() < 2) {
            Toast.makeText(getActivity(), "Add at least 2 points", Toast.LENGTH_LONG).show();
        } else if (LINE == null) {
            Toast.makeText(getActivity(), "First draw path", Toast.LENGTH_LONG).show();
        } else if (calendar == null || calendar.getTime() == null) {
            Toast.makeText(getActivity(), "Set date of event", Toast.LENGTH_LONG).show();
        } else {
            Log.d(TAG, "addNewEvent3");
            Event event = new Event(new Date(), dateStart, accId, title, LINE, potionsList.get(0), potionsList.get(potionsList.size() - 1));
            Log.d(TAG, "addNewEvent4");
            FirestoreService.getInstance(getActivity().getApplicationContext()).saveEvent(event, new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    Log.d(TAG, "DocumentSnapshot successfully updated!");
                    Toast.makeText(getActivity(), "Event successfuly added!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getActivity(), EventsOverview.class));
                }
            }, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(TAG, "Error while adding event", e);
                    Toast.makeText(getActivity(), "Error while creating event", Toast.LENGTH_LONG).show();
                }
            });
            Log.d(TAG, "addNewEvent5");
        }
        Log.d(TAG, "addNewEvent6");

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}


