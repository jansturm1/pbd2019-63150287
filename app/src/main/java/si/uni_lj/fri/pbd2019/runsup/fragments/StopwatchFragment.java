package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.services.FirestoreService;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class StopwatchFragment extends android.support.v4.app.Fragment {
    private static final String TAG = "StopwatchFragment";

    BroadcastReceiver mBroadcastReceiver;

    private Intent mTrackerServiceStart;
    private Intent mTrackerServiceContinue;
    private Intent mTrackerServicePause;
    private Intent mTrackerServiceStop;
    private Intent mTrackerServiceUpdateSportActivity;

    private static String lastButtonClicked = null;
    private static Integer state;

    private static Integer sportActivity = SportActivities.RUNNING;
    private static long duration;
    private static double distance;
    private static double pace;
    private static double calories;
    private static Long workoutId;
    private static Location location;

    private static ArrayList<List<Location>> finalPositionList = new ArrayList<>();

    public StopwatchFragment() {
    }

    public static StopwatchFragment newInstance(String param1, String param2) {
        StopwatchFragment fragment = new StopwatchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("RunsUp!");  // set actionBar title
        return inflater.inflate(R.layout.fragment_stopwatch, container, false);
    }

    private String getNextButtonName() {
        String btnName = null;

        if (lastButtonClicked == null || lastButtonClicked.equals(getString(R.string.stopwatch_endworkout))) {
            btnName = getString(R.string.stopwatch_start);
        } else if (lastButtonClicked.equals(getString(R.string.stopwatch_start))) {
            btnName = getString(R.string.stopwatch_stop);
        } else if (lastButtonClicked.equals(getString(R.string.stopwatch_stop))) {
            btnName = getString(R.string.stopwatch_continue);

        } else if (lastButtonClicked.equals(getString(R.string.stopwatch_continue))) {
            btnName = getString(R.string.stopwatch_stop);
        }
        return btnName;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkPermissions();
        initIntents();
        initBroadcastReceiver();
        initButtons();
    }

    @Override
    public void onResume() {
        super.onResume();

        final Button buttonStart = (Button) getView().findViewById(R.id.button_stopwatch_start);
        final Button buttonEndWorkout = (Button) getView().findViewById(R.id.button_stopwatch_endworkout);

        updateCounters(sportActivity, duration, distance, pace, calories);


        Log.d(TAG, "getNextButtonName(): " + getNextButtonName());

        buttonStart.setText(getNextButtonName());

        if (getNextButtonName().equals(getString(R.string.stopwatch_continue))) {
            buttonEndWorkout.setVisibility(View.VISIBLE);
        } else if (getNextButtonName().equals(getString(R.string.stopwatch_start))) {
            buttonEndWorkout.setVisibility(View.GONE);
        }

        //FirestoreService.getInstance(getContext()).saveToDb();
        //FirestoreService.getInstance(getContext()).readData();

        getActivity().startService(new Intent(getActivity(), TrackerService.class));   // preveri ce je ok
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(Constants.TICK));
    }

    @Override
    public void onPause() {
        super.onPause();

        if (lastButtonClicked == null || lastButtonClicked.equals(getString(R.string.stopwatch_stop)) || lastButtonClicked.equals(getString(R.string.stopwatch_endworkout))) {
            getActivity().stopService(new Intent(getActivity(), TrackerService.class));
        }
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_main_action_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            case R.id.menu_main_action_sync_server:
                User loggedInUser = MainHelper.getLoggedInUser(getActivity());
                FirestoreService.getInstance(getActivity().getApplicationContext()).syncWithServer(loggedInUser);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkPermissions() {
        Log.d(TAG, "checkPermissions: 1");
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "checkPermissions: 2");

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Log.d(TAG, "checkPermissions: 3");

            } else {
                Log.d(TAG, "checkPermissions: 4");

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            Log.d(TAG, "checkPermissions: 5");

            // Permission has already been granted
        }
        Log.d(TAG, "checkPermissions: 6");
    }

    public void jumpToWorkoutDetail(View view) {

    }

    private void initBroadcastReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Constants.TICK.equals(intent.getAction())) {
                    duration = intent.getLongExtra("duration", 1337);
                    pace = intent.getDoubleExtra("pace", 1337.0);
                    distance = intent.getDoubleExtra("distance", 1337.0);
                    calories = intent.getDoubleExtra("calories", 1337.0);
                    sportActivity = intent.getIntExtra("sportActivity", 1337);
                    location = intent.getParcelableExtra("location");
                    workoutId = intent.getLongExtra("workoutId", 1337L);
                    location = intent.getParcelableExtra("location");

                    //Log.d(TAG, "#location= " + location);
                    Log.d("MOJ", "workoutId= " + workoutId);
                    Log.d("MOJ", "durationSec= " + duration);
                    Log.d("MOJ", "formated= " + MainHelper.formatDuration(duration));

                    updateCounters(sportActivity, duration, distance, pace, calories);
                }
            }
        };
    }

    private void initButtons() {
        final Button buttonStart = (Button) getView().findViewById(R.id.button_stopwatch_start);
        final Button buttonEndWorkout = (Button) getView().findViewById(R.id.button_stopwatch_endworkout);
        final Button buttonSelectSport = (Button) getView().findViewById(R.id.button_stopwatch_selectsport);
        final Button buttonActiveWorkout = (Button) getView().findViewById(R.id.button_stopwatch_activeworkout);
        buttonSelectSport.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                onButtonSelectSportPressed();
            }
        });
        buttonStart.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                initIntents();

                if (buttonStart.getText().equals(getString(R.string.stopwatch_start))) {
                    buttonStart.setText(R.string.stopwatch_stop);
                    lastButtonClicked = getString(R.string.stopwatch_start);
                    getActivity().startService(mTrackerServiceStart);

                } else if (buttonStart.getText().equals(getString(R.string.stopwatch_stop))) {
                    buttonStart.setText(R.string.stopwatch_continue);
                    buttonEndWorkout.setVisibility(View.VISIBLE);
                    lastButtonClicked = getString(R.string.stopwatch_stop);
                    getActivity().startService(mTrackerServicePause);

                } else if (buttonStart.getText().equals(getString(R.string.stopwatch_continue))) {
                    buttonStart.setText(R.string.stopwatch_stop);
                    buttonEndWorkout.setVisibility(View.GONE);
                    lastButtonClicked = getString(R.string.stopwatch_continue);
                    getActivity().startService(mTrackerServiceContinue);
                }
            }
        });

        buttonEndWorkout.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Do you really want to end workout and reset counters?")
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                lastButtonClicked = getString(R.string.stopwatch_endworkout);
                                resetCounters();
                                getActivity().startService(mTrackerServiceStop);
                                LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
                                startActivity(getWorkoutDetailIntent());
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
            }
        });
        buttonActiveWorkout.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ActiveWorkoutMapActivity.class));
            }
        });
    }

    public void onButtonSelectSportPressed() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setIcon(R.drawable.googleg_standard_color_18);
        builderSingle.setTitle("Select Sport Activity");

        String[] activies = new String[]{"Running", "Walking", "Cycling"};

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice);

        for (String activity : activies) {
            arrayAdapter.add(activity);
        }

        builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Button buttonSelectSport = (Button) getView().findViewById(R.id.button_stopwatch_selectsport);
                if (which == SportActivities.RUNNING) {
                    buttonSelectSport.setText("Running");
                } else if (which == SportActivities.WALKING) {
                    buttonSelectSport.setText("Walking");
                } else if (which == SportActivities.CYCLING) {
                    buttonSelectSport.setText("Cycling");
                }
                sportActivity = which;  // update current sportActivity
                Intent intent = new Intent(getActivity(), TrackerService.class);
                intent.setAction(Constants.UPDATE_SPORT_ACTIVITY);
                intent.putExtra("sportActivity", sportActivity);
                Log.d(TAG, "alert(): sportActivity= " + sportActivity);
                getActivity().startService(intent);
            }
        });
        builderSingle.show();
    }

    private void updateCounters(int sportActivity, long duration, double distance, double pace, double calories) {

        Log.d(TAG, "updateWorkoutCounters");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        int distanceUnitInt = Integer.parseInt(sharedPref.getString("unit", "0"));

        String distanceUnit = distanceUnitInt == 0 ? getString(R.string.all_labeldistanceunitkilometers) : getString(R.string.all_labeldistanceunitmiles);
        String paceUnit = getString(R.string.all_min) + "/" + distanceUnit; // ce bo error more bit slash namest pike
        String caloriesUnit = getString(R.string.all_labelcaloriesunit);

        double distanceFinal = distance;
        double paceFinal = pace;

        if (distanceUnitInt == 1) {
            distanceFinal = MainHelper.kmphToMiph(distance);
            paceFinal = MainHelper.minpkmToMinpmi(pace);
        }

        TextView tvDuration = (TextView) getView().findViewById(R.id.textview_stopwatch_duration);
        tvDuration.setText(MainHelper.formatDuration(duration));

        TextView tvPace = (TextView) getView().findViewById(R.id.textview_stopwatch_pace);
        tvPace.setText(MainHelper.formatPace(paceFinal));
        TextView tvPaceUnit = (TextView) getView().findViewById(R.id.textview_stopwatch_unitpace);
        tvPaceUnit.setText(paceUnit);

        TextView tvDistance = (TextView) getView().findViewById(R.id.textview_stopwatch_distance);
        tvDistance.setText(MainHelper.formatDistance(distanceFinal));
        TextView tvDistanceUnit = (TextView) getView().findViewById(R.id.textview_stopwatch_distanceunit);
        tvDistanceUnit.setText(distanceUnit);

        TextView tvCalories = (TextView) getView().findViewById(R.id.textview_stopwatch_calories);
        tvCalories.setText(MainHelper.formatCalories(calories));
        TextView tvCaloriesUnit = (TextView) getView().findViewById(R.id.textview_stopwatch_unitcalories);
        tvCaloriesUnit.setText(caloriesUnit);
    }

    private void resetCounters() {
        sportActivity = SportActivities.RUNNING;
        duration = 0;
        distance = 0.0;
        pace = 0.0;
        calories = 0.0;
    }

    private Intent getWorkoutDetailIntent() {
        Intent intent = new Intent(getActivity(), WorkoutDetailActivity.class);
        intent.putExtra("workoutId", workoutId);
        intent.putExtra("sportActivity", sportActivity);
        intent.putExtra("duration", duration);
        intent.putExtra("distance", distance);
        intent.putExtra("calories", calories);
        intent.putExtra("pace", pace);
        intent.putExtra("state", state);
        //intent.putExtra("finalPositionList", new ArrayList<List<Location>>());
        return intent;
    }

    private void initIntents() {
        mTrackerServiceStart = new Intent(getActivity(), TrackerService.class);
        mTrackerServiceStart.setAction(Constants.COMMAND_START);
        mTrackerServiceStart.putExtra("sportActivity", sportActivity);

        mTrackerServiceContinue = new Intent(getActivity(), TrackerService.class);
        mTrackerServiceContinue.setAction(Constants.COMMAND_CONTINUE);
        mTrackerServiceStart.putExtra("sportActivity", sportActivity);

        mTrackerServicePause = new Intent(getActivity(), TrackerService.class);
        mTrackerServicePause.setAction(Constants.COMMAND_PAUSE);

        mTrackerServiceStop = new Intent(getActivity(), TrackerService.class);
        mTrackerServiceStop.setAction(Constants.COMMAND_STOP);

        mTrackerServiceUpdateSportActivity = new Intent(getActivity(), TrackerService.class);
        mTrackerServiceUpdateSportActivity.setAction(Constants.UPDATE_SPORT_ACTIVITY);
    }
}