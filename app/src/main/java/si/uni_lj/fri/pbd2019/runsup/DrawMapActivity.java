package si.uni_lj.fri.pbd2019.runsup;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import si.uni_lj.fri.pbd2019.runsup.fragments.DrawMapBottomDialog;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.pojo.Direction;
import si.uni_lj.fri.pbd2019.runsup.services.DirectionsService;

public class DrawMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static String TAG = "DrawMapActivity";
    private GoogleMap mMap;
    private Button buttonClearMap;
    private Button buttonDrawPolyline;
    private Button buttonAddEvent;
    private Button buttonviewEvents;
    private TextView textViewDateStart;
    private Button buttonGoHome;

    private DirectionsService directionsService;
    private Polyline polyline;
    private String LINE;
    private User user;

    private DatePickerDialog.OnDateSetListener date;
    private DatePickerDialog mDatePickerDialog;
    private Calendar calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_draw_map);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_drawMap);

        user = MainHelper.getLoggedInUser(getApplicationContext());
        Log.d(TAG, "loggedInUser= " + (user != null ? user.toString() : "null"));

        directionsService = DirectionsService.getInstance(this);

        buttonClearMap = findViewById(R.id.drawMap_button_clearMmap);
        buttonClearMap.setVisibility(View.GONE);
        buttonClearMap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mMap != null) {
                    directionsService.removeAllPositions();
                    mMap.clear();
                    buttonDrawPolyline.setVisibility(View.GONE);
                    buttonClearMap.setVisibility(View.GONE);
                    buttonAddEvent.setVisibility(View.GONE);
                }
            }
        });

        buttonviewEvents = findViewById(R.id.drawMap_button_view_events);
        buttonviewEvents.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(DrawMapActivity.this, EventsOverview.class));
            }
        });

        buttonAddEvent = findViewById(R.id.drawMap_button_add_event);
        buttonAddEvent.setVisibility(View.GONE);
        //buttonAddEvent.setVisibility(View.GONE);
        buttonAddEvent.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialogBox();
                //showDateTimePicker();

            }
        });

        buttonDrawPolyline = findViewById(R.id.drawMap_button_drawPolyline);
        buttonDrawPolyline.setVisibility(View.GONE);
        buttonDrawPolyline.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mMap != null) {
                    Log.d(TAG, "drawMe!!");
                    drawPathBetweenMarkers();
                }
            }
        });

        mapFragment.getMapAsync(this);
    }

    private void showDialogBox() {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString("LINE", LINE);
        editor.putString("accId", (user != null ? user.getAccId() : null));
        editor.apply();

        DrawMapBottomDialog bottomSheetFragment = new DrawMapBottomDialog();
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) {
            mMap.clear(); // ko pridemo nazaj z drugih pogledov, resetiramo, kar smo prej narisal :)
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "map ready");
        mMap = googleMap;
        //mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.google_maps_style));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46.056946, 14.505752), 12));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                directionsService.addPosition(point);
                Marker marker = mMap.addMarker(new MarkerOptions().position(point).title(Integer.toString(directionsService.getPositionListSize())));
                directionsService.addPosition(marker.getPosition());
                buttonDrawPolyline.setVisibility(View.VISIBLE);
                buttonClearMap.setVisibility(View.VISIBLE);
                buttonAddEvent.setVisibility(View.VISIBLE);
            }
        });
    }

    private void drawPathBetweenMarkers() {

        directionsService.fetchPolyline(mMap, new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "Rest api call: failed!");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Rest api call: Unsuccessful response!");
                } else {
                    try {
                        LINE = null;
                        Gson gson = new Gson();
                        Direction direction = gson.fromJson(response.body().string(), Direction.class);
                        String points = null;


                        if (direction.getRoutes() != null && direction.getRoutes().size() > 0) {
                            points = direction.getRoutes().get(0).getOverViewPolyline().getPoints();
                            LINE = points;
                            Log.d(TAG, "LINE= " + LINE);

                            if (direction.getRoutes().get(0).getLegs() != null && direction.getRoutes().get(0).getLegs().size() > 0) {
                                Log.d(TAG, "distance= " + direction.getRoutes().get(0).getLegs().get(0).getDistance());

                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (LINE != null) {
                                    drawMap();
                                    buttonAddEvent.setVisibility(View.VISIBLE);
                                    buttonDrawPolyline.setVisibility(View.GONE);
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public void drawMap() {
        if (LINE != null) {
            List<LatLng> line = PolyUtil.decode(LINE);

            if (line.size() >= 2) {
                List<LatLng> positionList = directionsService.getPotionsList();
                mMap.clear();
                polyline = mMap.addPolyline(new PolylineOptions()
                        .addAll(line)
                        .color(Color.BLACK));
                mMap.addMarker(new MarkerOptions().title("Start").position(positionList.get(0)));
                mMap.addMarker(new MarkerOptions().title("End").position(positionList.get(positionList.size() - 1)));
                moveCamera(positionList);
            }
        }
    }

    public void moveCamera(List<LatLng> positionList) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (LatLng position : positionList) {
            builder.include(position);
        }

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.animateCamera(cu);

    }

    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        calendar = Calendar.getInstance();
        new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d(TAG, "year= " + year);
                calendar.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(DrawMapActivity.this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        Log.v(TAG, "The choosen one " + calendar.getTime());

                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Log.d(TAG, "before format= " + calendar.getTime());
                        String dateStartFormated = dateFormat.format(calendar.getTime());
                        textViewDateStart.setText(dateStartFormated);

                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }


}
