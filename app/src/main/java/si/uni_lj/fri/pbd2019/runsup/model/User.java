package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class User {

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private Long id;

    @DatabaseField
    private String accId;

    @DatabaseField
    private String authToken;

    @ForeignCollectionField()
    ForeignCollection<Workout> workouts;

    public User() {
    }

    public User(String accId) {
        this.accId = accId;
    }

    public User(String accId, String authToken) {
        this.accId = accId;
        this.authToken = authToken;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public ForeignCollection<Workout> getWorkouts() {
        return workouts;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", accId='" + accId + '\'' +
                ", authToken='" + authToken + '\'' +
                '}';
    }
}
