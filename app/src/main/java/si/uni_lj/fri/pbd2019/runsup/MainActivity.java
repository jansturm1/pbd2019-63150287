package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "MainActivity";
    private Bitmap mBitmap = null;
    private static ImageView ivProfilePhoto;
    private static TextView tvFullName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d(TAG, "onCreate");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        ivProfilePhoto = (ImageView) header.findViewById(R.id.menu_loggedInUserImage);
        tvFullName = (TextView) header.findViewById(R.id.menu_loggedInUserFullName);

        ivProfilePhoto.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.flContent, new StopwatchFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_workout);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void renderUserLoggedInPhotoAndName() {

        Log.d(TAG, "private void renderUserLoggedInPhotoAndName()");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean isUserLoggedIn = sharedPref.getBoolean(getString(R.string.shared_pref_userSignedIn), false);
        String fullName = sharedPref.getString(getString(R.string.shared_pref_fullName), "");
        String profilePhotoUrl = sharedPref.getString(getString(R.string.shared_pref_profilePhoto), "");

        Log.d(TAG, "fullName= " + fullName + ", url= " + profilePhotoUrl);

        if (isUserLoggedIn) {
            //Log.d(TAG, "if (fullName != null && profilePhotoUrl != null)");
//            Bitmap bitmap = MainHelper.getBitmapFromUrl("https://miro.medium.com/max/480/1*d69DKqFDwBZn_23mizMWcQ.png");
//            Log.d(TAG, "bitmap= " + bitmap);
//            if (bitmap != null) {
//                Log.d(TAG, "if (bitmap != null)");
//                ivProfilePhoto.setImageBitmap(bitmap);
//            }
            tvFullName.setText(fullName);

        } else {
            tvFullName.setText("No user logged in");
            Log.d(TAG, "cannot find user from MainHelper !!");
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_workout) {
            getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.flContent, new StopwatchFragment()).commit();
        } else if (id == R.id.nav_history) {
            getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.flContent, new HistoryFragment()).commit();
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (id == R.id.nav_about) {
            getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.flContent, new AboutFragment()).commit();
        } else if (id == R.id.nav_drawMap) {
            startActivity(new Intent(this, DrawMapActivity.class));
        } else if (id == R.id.nav_eventsOverview) {
            startActivity(new Intent(this, EventsOverview.class));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        renderUserLoggedInPhotoAndName();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
