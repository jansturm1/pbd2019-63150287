package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.pojo.PersonAttribute;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LoginActivity";

    private static FirebaseAuth mAuth;
    private LoginListAdapter mLoginListAdapterr;
    private static final int RC_SIGN_IN = 1337;
    GoogleApiClient mGoogleApiClient;
    private static SignInButton googleSignIn;
    private static Button googleSignOut;
    private static DatabaseService dbService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, getSignInOptions())
                .build();

        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initButtons();
        dbService = DatabaseService.getInstance(getApplicationContext());

        ListView listView = (ListView) findViewById(R.id.activity_login_listview);
        ArrayList<PersonAttribute> personAttributeList = new ArrayList<>(Arrays.asList(
                new PersonAttribute("Weight", 80, "kg"),
                new PersonAttribute("Age", 25, "years"),
                new PersonAttribute("Height", 180, "cm"))
        );
        mLoginListAdapterr = new LoginListAdapter(this, personAttributeList);
        listView.setAdapter(mLoginListAdapterr);
    }

    protected void login() {
        startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient), RC_SIGN_IN);
    }

    protected void logout() {
        mAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        saveUserDbAndSharedPreferences();
        toggleButtons();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d(TAG, "onConnectionFailed= " + result.getErrorMessage());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "result= " + result.getStatus());
            if (result.isSuccess()) {
                firebaseAuthWithGoogle(result.getSignInAccount());
            } else {
                Toast.makeText(this, "cannot login", Toast.LENGTH_LONG);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential authCredential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(authCredential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "on login complete= " + (task.isSuccessful() ? "success" : "fail"));
                        toggleButtons();
                        saveUserDbAndSharedPreferences();
                    }
                });
    }

    private FirebaseUser getCurrentUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Log.d(TAG, "user is null= " + (user == null ? true : false));
        if (user != null) {
            Log.d(TAG, "uid= " + user.getUid() + "name= " + user.getDisplayName() + ", email= " + user.getEmail() + ", photo_url= " + user.getPhotoUrl());
        }
        return user;
    }

    private GoogleSignInOptions getSignInOptions() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
    }

    private void initButtons() {
        googleSignIn = (SignInButton) findViewById(R.id.login_sign_in);
        googleSignOut = (Button) findViewById(R.id.login_sign_out);
        toggleButtons();
        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainHelper.isInternetAvailable(getApplicationContext())) {
                    login();
                } else {
                    Log.d(TAG, "Turn on internet connection");
                    Toast.makeText(LoginActivity.this, "Turn on internet connection", Toast.LENGTH_SHORT);
                }
            }
        });
        googleSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void saveUserDbAndSharedPreferences() {

        final FirebaseUser firebaseUser = getCurrentUser();

        if (firebaseUser != null) {
            firebaseUser.getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                @Override
                public void onSuccess(GetTokenResult result) {
                    String idToken = result.getToken();
                    String accId = firebaseUser.getUid();
                    User userInDb = dbService.findUserByAccId(accId);

                    if (userInDb == null) {
                        User userNew = new User(firebaseUser.getUid(), idToken);
                        try {
                            dbService.insertUser(userNew);
                            userInDb = userNew;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    } else {
                        userInDb.setAuthToken(idToken);
                        dbService.updateUser(userInDb);
                    }
                    setSharedPreferences(userInDb, firebaseUser);
                }
            });
        } else {
            setSharedPreferences(null, null);
        }
    }

    private void setSharedPreferences(User userInDb, FirebaseUser firebaseUser) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        if (firebaseUser != null) {
            Log.d(TAG, "firebaseUser != null");

            if (userInDb != null) {
                Log.d(TAG, "userInDb= " + userInDb.toString());
            }

            editor.putLong(getString(R.string.shared_pref_userId), (userInDb != null ? userInDb.getId() : null));
            editor.putString(getString(R.string.shared_pref_fullName), firebaseUser.getDisplayName());
            editor.putString(getString(R.string.shared_pref_profilePhoto), firebaseUser.getPhotoUrl().toString());
            editor.putBoolean(getString(R.string.shared_pref_userSignedIn), true);
        } else {
            Log.d(TAG, "firebaseUser == null");
            editor.putBoolean(getString(R.string.shared_pref_userSignedIn), false);
        }
        editor.apply();
    }

    private void toggleButtons() {
        if (getCurrentUser() == null) {
            googleSignIn.setVisibility(View.VISIBLE);
            googleSignOut.setVisibility(View.GONE);
        } else {
            googleSignIn.setVisibility(View.GONE);
            googleSignOut.setVisibility(View.VISIBLE);
        }
    }
}