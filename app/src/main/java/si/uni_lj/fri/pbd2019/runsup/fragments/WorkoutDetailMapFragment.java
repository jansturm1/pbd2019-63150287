package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.pojo.Event;
import si.uni_lj.fri.pbd2019.runsup.services.DirectionsService;
import si.uni_lj.fri.pbd2019.runsup.services.FirestoreService;

public class WorkoutDetailMapFragment extends Fragment {

    private static String TAG = "WorkoutDetailMapFragment";
    private String accId;
    private String LINE;
    private Calendar calendar;
    private TextView tvDateStart;
    private Button buttonSetStartDate;

    private DirectionsService directionsService;


    public WorkoutDetailMapFragment() {
    }

    public static WorkoutDetailMapFragment newInstance(String param1, String param2) {
        WorkoutDetailMapFragment fragment = new WorkoutDetailMapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        directionsService = DirectionsService.getInstance(getActivity());


        accId = getArguments().getString("accId");
        LINE = getArguments().getString("LINE");


        return inflater.inflate(R.layout.fragment_workout_detail_map, viewGroup, false);
    }

    public void showDateTimePicker() {
        Log.d(TAG, "2");
        final Calendar currentDate = Calendar.getInstance();
        calendar = Calendar.getInstance();
        new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("", "year= " + year);
                calendar.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(getActivity(), R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        Log.v("", "The choosen one " + calendar.getTime());

                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Log.d("", "before format= " + calendar.getTime());
                        String dateStartFormated = dateFormat.format(calendar.getTime());
                        //textViewDateStart.setText(dateStartFormated);

                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void addNewEvent(String title, Date dateStart) {
        List<LatLng> potionsList = directionsService.getPotionsList();
        if (directionsService.getPositionListSize() >= 2 && LINE != null && accId != null) {
            Event event = new Event(new Date(), dateStart, accId, title, LINE, potionsList.get(0), potionsList.get(potionsList.size() - 1));
            FirestoreService.getInstance(getActivity().getApplicationContext()).saveEvent(event, new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    Log.d(TAG, "DocumentSnapshot successfully updated!");
                }
            }, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(TAG, "Error updating document", e);
                }
            });
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
