package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.pojo.TimeSlot;

public class TrackerService extends Service {

    private final String TAG = "TrackerService.class";

    private final IBinder mBinder = new LocalBinder();
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Runnable runnable;
    private Handler handler;

    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;


    private static double pace = 0.0;
    private static int state;
    private static long duration;
    private static double distance;
    private static double calories;
    private static long timeFillingSpeedListInMilliSec = 0;
    private static long sessionNumber = 0;

    private static Workout dbWorkout = null;
    private static String actionCurr;
    private static Integer sportActivity;


    private static Location mPreviousLocation;
    private static Location mCurrentLocation;
    private static Boolean mRequestingLocationUpdates = false;

    private static DatabaseService databaseService = null;

    private static ArrayList<Location> list;
    private static List<Float> speedList;

    private static TimeSlot runtime;
    private static TimeSlot timeBetweenTwoLocations;

    private static int countMissedLocations = 0;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public TrackerService getService() {
            return TrackerService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        initBroadCastLoop();
        createLocationRequest();
        createLocationCallback();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            actionCurr = intent.getAction();
            setStateAndPersistWorkout();

            if (actionCurr.equals(Constants.COMMAND_START) || actionCurr.equals(Constants.COMMAND_CONTINUE)) {
                handleStartContinue(intent);
            } else if (actionCurr.equals(Constants.COMMAND_PAUSE) || actionCurr.equals(Constants.COMMAND_STOP)) {
                handleStopPause();
            } else if (actionCurr.equals(Constants.UPDATE_SPORT_ACTIVITY)) {
                //The service accepts si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY
                //If needed, the service updates the workout (also in the database)
                sportActivity = intent.getIntExtra("sportActivity", 1338);
                Log.d(TAG, "onStartCommand(): sportActivity= " + sportActivity);

                if (dbWorkout != null) { // ce workout za obstaja
                    updateWorkoutInDb();
                }
            }
        }
        return START_STICKY;
    }

    private void handleStopPause() {
        // dodaj pot v seznam
        stopLocationUpdates();
        handler.removeCallbacks(runnable); // mogoce null pointer exception ?    // stop sending periodic broadcast

        // Dodaj duration, ki se ni upostevan
        runtime.setToMillis(SystemClock.uptimeMillis());
        duration += runtime.getDurationMillis();
    }

    private void handleStartContinue(Intent intent) {
        if (actionCurr.equals(Constants.COMMAND_START)) {
            sportActivity = intent.getIntExtra("sportActivity", 1337);
            Log.d(TAG, "handleStartContinue(): sportActivity= " + sportActivity);

            list = new ArrayList<>();
            speedList = new ArrayList<>();
            timeBetweenTwoLocations = new TimeSlot();
            mPreviousLocation = null;
            runtime = new TimeSlot();
            databaseService = DatabaseService.getInstance(getApplicationContext());
            countMissedLocations = 0;

            pace = 0.0;
            duration = 0;
            distance = 0.0;
            calories = 0.0;
            timeFillingSpeedListInMilliSec = 0;

            sessionNumber = 0L;

            dbWorkout = null;
            insertWorkoutInDb();
        } else {
            sessionNumber++;
        }
        list = new ArrayList<>();
        runtime.setFromMillis(SystemClock.uptimeMillis());
        handler.postDelayed(runnable, Constants.DELAY_CLIENT);
        startLocationUpdates(); // mogoce more bit v on create ce metoda crkne ?
    }

    private void setStateAndPersistWorkout() {
        int statePrev = state;
        switch (actionCurr) {
            case Constants.COMMAND_START:
                state = Workout.statusUnknown;
                break;
            case Constants.COMMAND_CONTINUE:
                state = Workout.statusUnknown;
                break;
            case Constants.COMMAND_PAUSE:
                state = Workout.statusPaused;
                break;
            case Constants.COMMAND_STOP:
                state = Workout.statusEnded;
                break;
        }
        if (statePrev != state && !actionCurr.equals(Constants.COMMAND_START)) {  // ker se ustvari on handleStart
            updateWorkoutInDb();
        }
        Log.d(TAG, "#prev= " + statePrev + ", #state= " + state);
    }


    private void updateLocation(Location newLocation) {
        // razdalja v m
        float distanceCurr = MainHelper.getDistanceLastTwoLocations(mPreviousLocation, newLocation);

        if (list.isEmpty()) {
            // Handle corner case with resume
            if (actionCurr.equals(Constants.COMMAND_CONTINUE) && distanceCurr <= Constants.CONTINUE_MAX_DISTANCE) {
                distance += distanceCurr;
            }
        } else {
            // Povecaj distance
            distance += distanceCurr;
            // Cas od prejsnje lokacije
            timeBetweenTwoLocations.setToMillis(SystemClock.uptimeMillis());   // pride 2.lokacija
            long durationBetweenLocations = timeBetweenTwoLocations.getDurationMillis();
            // povecaj duration
            timeFillingSpeedListInMilliSec += durationBetweenLocations;  // dodamo trajanje

            // Hitrost
            float speed = SportActivities.calculateSpeed(distanceCurr, durationBetweenLocations);
            speedList.add(speed);
        }

        // Dodaj lokacijo
        list.add(newLocation);
        // Shrani cas lokacije
        timeBetweenTwoLocations.setFromMillis(SystemClock.uptimeMillis());
        mPreviousLocation = newLocation;

        pace = SportActivities.calculatePace(getDistance(), getDuration());


        try {
            List<Float> speedListClone = new ArrayList<>(speedList);
            updateWorkoutInDb();
            insertGpsPointInDb(speedListClone);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void broadCastDataToClient() {
        //ce hocmo da dela ok periodicno mormo vsako sekundo znova pridobit nove podatke
        LocalBroadcastManager.getInstance(this).sendBroadcast(getIntentCollectedData(sportActivity));
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mRequestingLocationUpdates = true;
        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private void stopLocationUpdates() {
        if (mRequestingLocationUpdates) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
            mRequestingLocationUpdates = false;
        }
    }

    private void initBroadCastLoop() {
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                // Update time
                runtime.setToMillis(SystemClock.uptimeMillis());
                broadCastDataToClient();
                handler.postDelayed(this, Constants.DELAY_CLIENT);
            }
        };
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.LOCATION_REQUEST_INTERVAL);
        mLocationRequest.setSmallestDisplacement(Constants.LOCATION_SMALLEST_DISPLACEMENT);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                updateLocation(mCurrentLocation);
            }
        };
    }

    private void updateCalories() {
        double timeFillingSpeedListInHours = MainHelper.convMillisecToHour(timeFillingSpeedListInMilliSec);
        double newCalories = SportActivities.countCalories(sportActivity, Constants.WEIGHT, speedList, timeFillingSpeedListInHours);

        calories += newCalories;
        speedList.clear();
        timeFillingSpeedListInMilliSec = 0;
    }

    private Intent getIntentCollectedData(int sportActivity) {

        if (mCurrentLocation == null) {
            countMissedLocations++;
        } else {
            countMissedLocations = 0;
        }
        if (countMissedLocations > 10) {
            updateWorkoutInDb();
        }

        // Celoten cas = cas od prej + cas od klika na start
        Long durationSec = Double.valueOf(MainHelper.convMillisecToSec(getDuration())).longValue();
        Intent intent = new Intent(Constants.TICK);
        intent.putExtra("workoutId", (dbWorkout != null ? dbWorkout.getId() : null));
        intent.putExtra("duration", durationSec);
        intent.putExtra("distance", distance);
        intent.putExtra("pace", getPace());
        intent.putExtra("calories", getCalories());
        intent.putExtra("state", state);
        intent.putExtra("sportActivity", sportActivity);
        intent.putExtra("location", mCurrentLocation);


        mCurrentLocation = null; // da lahko zaznamo koliko casa ni prislo do spremembe lokacije

        return intent;
    }


    private void insertWorkoutInDb() {
        try {
            double totalCalories = 0;
            double paceAvg = 0;
            User loggedInUser = null;

            Long newWorkoutId = 0L;
            Workout lastWorkoutInDb = databaseService.findLastWorkout();

            if (lastWorkoutInDb != null) {
                newWorkoutId = lastWorkoutInDb.getId() + 1;     // TODO: preveri ce je ok
            }

            loggedInUser = MainHelper.getLoggedInUser(getApplicationContext());
            String title = "Workout " + Long.toString(newWorkoutId);

            Workout workoutToInsert = new Workout(loggedInUser, title, new Date(), new Date(), state, distance,
                    duration, totalCalories, paceAvg, getSportActivity());
            databaseService.insertWorkout(workoutToInsert);

            dbWorkout = workoutToInsert;
        } catch (Exception ex) {

        }
    }

    private void updateWorkoutInDb() {
        try {
            double totalCalories = getCalories();
            double paceAvg = getPace();
            User loggedInUser = MainHelper.getLoggedInUser(getApplicationContext());

            dbWorkout.setUser(loggedInUser);          // dodano naknadno
            dbWorkout.setLastUpdate(new Date());      // dodano naknadno
            dbWorkout.setTotalCalories(totalCalories);
            dbWorkout.setPaceAvg(paceAvg);
            dbWorkout.setLastUpdate(new Date());
            dbWorkout.setDistance(getDistance());
            dbWorkout.setDuration(getDuration());
            dbWorkout.setSportActivity(getSportActivity());

            dbWorkout.setStatus(state);

            databaseService.updateWorkout(dbWorkout);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertGpsPointInDb(List<Float> speedListClone) {
        try {
            databaseService.insertGpsPoint(dbWorkout, sessionNumber, mCurrentLocation, getDuration(), speedListClone, getPace(), getCalories());
        } catch (SQLException e) {
        }
    }

    public static long getDuration() {
        return duration + runtime.getDurationMillis();
    }

    public double getDistance() {
        return distance;
    }

    public Integer getSportActivity() {
        return sportActivity;
    }

    public double getPace() {    // ne deluje se
        return pace;
    }

    public double getCalories() {
        updateCalories();
        return calories;
    }
}