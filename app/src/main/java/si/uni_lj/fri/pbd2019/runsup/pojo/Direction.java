package si.uni_lj.fri.pbd2019.runsup.pojo;

import java.util.List;

public class Direction {

    private List<Route> routes;

    public Direction() {
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "routes=" + routes +
                '}';
    }
}
