package si.uni_lj.fri.pbd2019.runsup.services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.pojo.Event;

public class FirestoreService {

    private static String TAG = "FirestoreService";

    private static FirestoreService firestoreUtilsInstance;
    private static FirebaseFirestore db;
    private static DatabaseService localDbService;

    private static CollectionReference workoutsCollectionRef;


    // TODO: v workoutServvice h vsakemu workout dodaj id ce je user prijavljen


    private FirestoreService() {
    }

    public static FirestoreService getInstance(Context context) {
        if (firestoreUtilsInstance == null) {
            firestoreUtilsInstance = new FirestoreService();
            localDbService = DatabaseService.getInstance(context);
            db = FirebaseFirestore.getInstance();
            FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                    .setTimestampsInSnapshotsEnabled(true)
                    .build();
            db.setFirestoreSettings(settings);

            workoutsCollectionRef = db.collection("workouts");
        }
        return firestoreUtilsInstance;
    }

    public void saveEvent(Event event, OnSuccessListener<QuerySnapshot> onSuccessListener, OnFailureListener onFailureListener) {
        if (event != null && event.getDateCreated() != null) {
            String id = Long.toString(event.getDateCreated().getTime());

            db.collection("events").document(id)
                    .set(event.toMap())
                    .addOnSuccessListener(onSuccessListener)
                    .addOnFailureListener(onFailureListener);

//            db.collection("events").document(id)
//                    .set(event.toMap())
//                    .addOnSuccessListener(new OnSuccessListener<Void>() {
//                        @Override
//                        public void onSuccess(Void aVoid) {
//                            Log.d(TAG, "DocumentSnapshot successfully updated!");
//                        }
//                    })
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            Log.w(TAG, "Error updating document", e);
//                        }
//                    });
        }
    }

    public void getEvents(OnCompleteListener<QuerySnapshot> callback) {

        db.collection("events").get().addOnCompleteListener(callback);


    }


    public void syncWithServer(User user) {
        Log.d(TAG, "sync started ...");

        if (user != null) {
            saveUserWorkoutsToDb(user);

            db.collection("workouts").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Map<String, Object> map = document.getData();
                            localDbService.createOrUpdateWorkout(mapToWorkout(map));

                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                }
            });


        } else {
            Log.d(TAG, "syncWithServer(): Pass non null User !");
        }
    }

    private void saveUserWorkoutsToDb(User user) {

        List<Workout> allWorkoutsList = localDbService.getAllWorkouts();
        List<Workout> userWorkoutsList = new ArrayList<>(); //  finished and not finished workouts

        for (Workout w : allWorkoutsList) {
            if ((w.getUser() != null) && (w.getUser().getAccId().equals(user.getAccId()))) {
                userWorkoutsList.add(w);
            }
        }

        WriteBatch batch = db.batch();


        for (Workout w : userWorkoutsList) {
            DocumentReference reference = db.collection("workouts").document(createNewWorkoutId(w));
            Map<String, Object> workoutMap = workoutToMap(w, user);
            if (workoutMap != null) {
                batch.set(reference, workoutToMap(w, user), SetOptions.merge());
            }
        }

        batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "Writting workouts to firebase completed");
            }
        });

    }

    private Map<String, Object> workoutToMap(Workout workout, User user) {

        if (workout == null || user == null) {
            Log.d(TAG, "#pass non null workout and user to run fun: Fire:workoutToMap()");
            return null;
        }

        Map<String, Object> workoutMap = new HashMap<>();

        workoutMap.put("accId", workout.getUser().getAccId());
        workoutMap.put("title", workout.getTitle());
        workoutMap.put("created", workout.getCreated());
        workoutMap.put("lastUpdate", workout.getLastUpdate());
        workoutMap.put("status", workout.getStatus());
        workoutMap.put("distance", workout.getDistance());
        workoutMap.put("duration", workout.getDuration());
        workoutMap.put("totalCalories", workout.getTotalCalories());
        workoutMap.put("paceAvg", workout.getPaceAvg());
        workoutMap.put("sportActivity", workout.getSportActivity());
        return workoutMap;
    }

    private Workout mapToWorkout(Map<String, Object> workoutMap) {
        Workout workout = new Workout();
        workout.setTitle((String) workoutMap.get("title"));

        //workout.setDuration(Long.parseLong((String) workoutMap.get("duration")));

        workout.setStatus((Integer) workoutMap.get("title"));

        workoutMap.get("created");
        workoutMap.get("lastUpdate");
        workoutMap.get("status");
        workoutMap.get("distance");
        workoutMap.get("duration");
        workoutMap.get("totalCalories");
        workoutMap.get("paceAvg");
        workoutMap.get("sportActivity");

        Log.d(TAG, "w= " + workout.toString());
        return workout;
    }

    private String createNewWorkoutId(Workout workout) {
        return (workout != null ? workout.getCreated().toString() : null);
    }
}
