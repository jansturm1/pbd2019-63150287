package si.uni_lj.fri.pbd2019.runsup;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.Set;

import si.uni_lj.fri.pbd2019.runsup.pojo.Event;

public class WorkoutInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public WorkoutInfoWindowAdapter(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.adapter_infowindow_route, null);

        Event event = (Event) marker.getTag();
        Set<String> participants = event.getParticipantsUid();
        int numOfParticipants = (participants != null ? participants.size() : 0);

        TextView tvTitle = view.findViewById(R.id.infowindow_title);
        TextView tvDistance = view.findViewById(R.id.infowindow_distance);
        TextView tvParticipants = view.findViewById(R.id.infowindow_participants);


        tvTitle.setText("Title: " + event.getTitle());
        tvDistance.setText("Distance: " + event.getDistance() + " km");
        tvParticipants.setText("Participants: " + numOfParticipants);


        return view;
    }

}
