package si.uni_lj.fri.pbd2019.runsup.model;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;

@DatabaseTable(tableName = "workouts")
public class Workout {

    // initial status of workout
    public static final int statusUnknown = 0;  // start
    // ended workout
    public static final int statusEnded = 1;    // end
    // paused workout
    public static final int statusPaused = 2;   // pause
    // deleted workout
    public static final int statusDeleted = 3;  // stop

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private Long id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private User user;

    @DatabaseField
    private String title;

    @DatabaseField
    private Date created;

    @DatabaseField
    private Date lastUpdate;

    @DatabaseField
    private Integer status;

    @DatabaseField
    private Double distance;

    @DatabaseField
    private Long duration;

    @DatabaseField
    private Double totalCalories;

    @DatabaseField
    private Double paceAvg;

    @DatabaseField
    private Integer sportActivity;

    @ForeignCollectionField
    private ForeignCollection<GpsPoint> gpsPoints;

    public Workout() {
    }

    public Workout(String title, int sportActivity) {
        this.title = title;
        this.sportActivity = sportActivity;
    }

    public Workout(User user, String title, Date created, Date lastUpdate, Integer status, Double distance, Long duration, Double totalCalories, Double paceAvg, Integer sportActivity) {
        this.user = user;
        this.title = title;
        this.created = created;
        this.lastUpdate = lastUpdate;
        this.status = status;
        this.distance = distance;
        this.duration = duration;
        this.totalCalories = totalCalories;
        this.paceAvg = paceAvg;
        this.sportActivity = sportActivity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Double getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(Double totalCalories) {
        this.totalCalories = totalCalories;
    }

    public Double getPaceAvg() {
        return paceAvg;
    }

    public void setPaceAvg(Double paceAvg) {
        this.paceAvg = paceAvg;
    }

    public Integer getSportActivity() {
        return sportActivity;
    }

    public void setSportActivity(Integer sportActivity) {
        this.sportActivity = sportActivity;
    }

    public ForeignCollection<GpsPoint> getGpsPoints() {
        return gpsPoints;
    }


    @Override
    public String toString() {
        return "Workout{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", title='" + title + '\'' +
                ", created=" + created +
                ", lastUpdate=" + lastUpdate +
                ", status=" + status +
                ", distance=" + distance +
                ", duration=" + duration +
                ", totalCalories=" + totalCalories +
                ", paceAvg=" + paceAvg +
                ", sportActivity=" + sportActivity +
                '}';
    }

    public String toStringWorkoutStatistics(Context context, int distanceUnitInt) {
        String activityName = null;
        String distanceUnit = distanceUnitInt == 0 ? context.getString(R.string.all_labeldistanceunitkilometers) : context.getString(R.string.all_labeldistanceunitmiles);
        String paceUnit = context.getString(R.string.all_min) + "/" + distanceUnit;
        String caloriesUnit = context.getString(R.string.all_labelcaloriesunit);

        Double distanceFinal = getDistance();
        Double paceFinal = getPaceAvg();

        if (distanceUnitInt == 1 && getDistance() != null && getPaceAvg() != null) {
            distanceFinal = MainHelper.kmphToMiph(getDistance());
            paceFinal = MainHelper.minpkmToMinpmi(getPaceAvg());
        }

        if (getSportActivity() == null || getDuration() == null || getDistance() == null ||
                getTotalCalories() == null || getPaceAvg() == null) {
            return null;
        }

        Log.d("Workout", "&sportActivity= " + getSportActivity());

        switch (getSportActivity()) {
            case 0:
                activityName = "Running";
                break;
            case 1:
                activityName = "Walking";
                break;
            case 2:
                activityName = "Cycling";
                break;
        }
        return String.format("%s %s | %s %s | %s %s | avg %s %s",
                MainHelper.formatDuration(getDuration()), activityName, MainHelper.formatDistance(distanceFinal),
                distanceUnit, MainHelper.formatCalories(getTotalCalories()), caloriesUnit, MainHelper.formatPace(paceFinal), paceUnit);
    }

    public String toStringWorkoutTime() {
        if (getCreated() == null) {
            return null;
        }
        //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss a", Locale.US);
        return dateFormat.format(getCreated());
    }

}
