package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "synclogs")
public class SyncLog {

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private Long id;

    public SyncLog() {
    }
}
