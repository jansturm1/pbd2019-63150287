package si.uni_lj.fri.pbd2019.runsup.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Route {

    @SerializedName("overview_polyline")
    private OverViewPolyline overViewPolyline;

    private List<Leg> legs;

    public Route() {
    }

    public OverViewPolyline getOverViewPolyline() {
        return overViewPolyline;
    }

    public void setOverViewPolyline(OverViewPolyline overViewPolyline) {
        this.overViewPolyline = overViewPolyline;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    @Override
    public String toString() {
        return "Route{" +
                "overViewPolyline=" + overViewPolyline +
                ", legs=" + legs +
                '}';
    }
}