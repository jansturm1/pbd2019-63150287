package si.uni_lj.fri.pbd2019.runsup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.MapsUtils;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class WorkoutDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static String TAG = "WorkoutDetailActivity";
    private BroadcastReceiver mBroadcastReceiver;

    private GoogleMap mMap;
    private static Integer sportActivity;
    private static Long workoutId;
    private static long duration;
    private static double distance;
    private static double pace;
    private static double calories;
    private static Context mContext;


    private static Workout workout = null;
    private static ArrayList<List<Location>> finalPositionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_workout_detail);
        mContext = this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // back button
        handleIntentData(getIntent());
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_workoutdetail_map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_workoutdetail_action_deleteworkout:
                deleteCurrentWorkout();
                return true;
            case R.id.menu_workoutdetail_action_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setScrollGesturesEnabled(false);
        uiSettings.setZoomGesturesEnabled(false);

        MapsUtils mapsUtils = null;


        if (workout != null) {

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    MapsUtils.drawMap(getApplicationContext(), googleMap, workout, false);
                }
            });

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    startActivity(new Intent(WorkoutDetailActivity.this, MapsActivity.class));
                }
            });
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.workout_detail, menu);
        return true;
    }

    private void handleIntentData(Intent intent) {

        workoutId = intent.getLongExtra("workoutId", 1337);
        duration = intent.getLongExtra("duration", 1337);
        pace = intent.getDoubleExtra("pace", 1337.0);
        distance = intent.getDoubleExtra("distance", 1337.0);
        calories = intent.getDoubleExtra("calories", 1337.0);
        sportActivity = intent.getIntExtra("sportActivity", 1337);

//        finalPositionList = intent.getParcelableArrayListExtra("finalPositionList");

        Log.d(TAG, "workoutId= " + workoutId);

        try {
            if (workoutId != null) {
                workout = DatabaseService.getInstance(getApplicationContext()).findWorkoutById(workoutId);
                Log.d(TAG, "workout= " + (workout != null ? workout.toString() : "null"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (workout != null) {

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            int distanceUnitInt = Integer.parseInt(sharedPref.getString("unit", "0"));
            String distanceUnit = distanceUnitInt == 0 ? getString(R.string.all_labeldistanceunitkilometers) : getString(R.string.all_labeldistanceunitmiles);
            String paceUnit = getString(R.string.all_min) + "." + distanceUnit; // ce bo error more bit slash namest pike
            String caloriesUnit = getString(R.string.all_labelcaloriesunit);


            List<GpsPoint> gpsPoints = new ArrayList<>(workout.getGpsPoints());
            boolean isMapVisible = ((gpsPoints != null && gpsPoints.size() >= 2) ? true : false);
            View mapWrapper = findViewById(R.id.workoutdetail_wrapper_maps);

            if (gpsPoints != null) {
                Log.d(TAG, "#gpsPointsSize= " + gpsPoints.size());
            }

            Log.d(TAG, "#isMapVisible= " + isMapVisible);

            if (isMapVisible) {
                mapWrapper.setVisibility(View.VISIBLE);
            } else {
                mapWrapper.setVisibility(View.GONE);
            }

            mapWrapper.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.d(TAG, "click()");
                }
            });


            Button editTextBtn = (Button) findViewById(R.id.workoutdetail_edit_title);
            editTextBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (workout != null) {
                        editWorkoutTitle(workout);
                    }
                }
            });

            if (workout.getSportActivity() != null) {
                TextView tvSportActivity = (TextView) findViewById(R.id.textview_workoutdetail_sportactivity);
                int sportActivity = workout.getSportActivity();
                String sportActivityName = null;

                if (sportActivity == SportActivities.RUNNING) {
                    sportActivityName = "Running";
                } else if (sportActivity == SportActivities.WALKING) {
                    sportActivityName = "Walking";
                } else if (sportActivity == SportActivities.CYCLING) {
                    sportActivityName = "Cycling";
                }
                tvSportActivity.setText(sportActivityName);
            }

            if (workout.getTitle() != null) {
                Log.d(TAG, "workoutTitle= " + workout.getTitle());
                TextView tvWorkoutTitle = (TextView) findViewById(R.id.textview_workoutdetail_workouttitle);
                tvWorkoutTitle.setText(workout.getTitle());
            }

            if (workout.getCreated() != null) {
                TextView tvActivityDate = (TextView) findViewById(R.id.textview_workoutdetail_activitydate);
                tvActivityDate.setText(workout.toStringWorkoutTime());
            }

            if (workout.getDuration() != null) {
                TextView tvDuration = (TextView) findViewById(R.id.textview_workoutdetail_valueduration);
                tvDuration.setText(MainHelper.formatDuration(workout.getDuration()));
            }

            if (workout.getPaceAvg() != null) {
                TextView tvPace = (TextView) findViewById(R.id.textview_workoutdetail_valueavgpace);
                tvPace.setText(MainHelper.formatPace(workout.getPaceAvg()) + " " + paceUnit);
            }

            if (workout.getDistance() != null) {
                TextView tvDistance = (TextView) findViewById(R.id.textview_workoutdetail_valuedistance);
                tvDistance.setText(MainHelper.formatDistance(workout.getDistance()) + " " + distanceUnit);
            }

            if (workout.getTotalCalories() != null) {
                TextView tvCalories = (TextView) findViewById(R.id.textview_workoutdetail_valuecalories);
                tvCalories.setText(MainHelper.formatCalories(workout.getTotalCalories()) + " " + caloriesUnit);
            }
        }
    }

    private void editWorkoutTitle(final Workout workout) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");

        final EditText input = new EditText(this);
        input.setText(workout.getTitle());

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                workout.setTitle(input.getText().toString());
                try {
                    DatabaseService.getInstance(getApplicationContext()).updateWorkout(workout);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                finish();
                startActivity(getIntent());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void deleteCurrentWorkout() {
        if (workoutId != null) {
            try {
                DatabaseService.getInstance(this).deleteWorkoutById(workoutId);
                finish();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
