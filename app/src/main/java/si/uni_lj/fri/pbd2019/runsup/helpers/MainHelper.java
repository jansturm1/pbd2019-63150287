package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import si.uni_lj.fri.pbd2019.runsup.Constants;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;

public final class MainHelper {

    // TODO

    public static double kmphToMiph(Double n) {
        if (n == null) {
            return 0.0;
        }
        return (1.0 * n * Constants.KM_TO_MI);
    }


    /**
     * return string of time in format HH:MM:SS
     *
     * @param time - in seconds
     */
    public static String formatDuration(Long time) {
        if (time == null) {
            return null;
        }
        long milliseconds = Double.valueOf(convSecToMillisec(time)).longValue();
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(milliseconds),
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) % TimeUnit.MINUTES.toSeconds(1));
    }

    public static float getDistanceLastTwoLocations(Location previousLocation, Location locationCurr) {
        if (previousLocation == null || locationCurr == null) {
            return 0;
        } else {
            return previousLocation.distanceTo(locationCurr);   // locationCurr was not added yet to list
        }
    }

    public static boolean isUserLoggedIn(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.shared_pref_userSignedIn), false);
    }

    public static User getLoggedInUser(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isSignedIn = sharedPref.getBoolean(context.getString(R.string.shared_pref_userSignedIn), false);

        User loggedInUser = null;

        if (isSignedIn) {
            Long id = sharedPref.getLong(context.getString(R.string.shared_pref_userId), -1);
            if (id != -1) {
                loggedInUser = DatabaseService.getInstance(context).findUserById(id);
            }
        }
        return loggedInUser;
    }


    public static boolean isInternetAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;
        }
        return false;
    }

    public static Bitmap getBitmapFromUrl(String src) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(src);
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bitmap;
    }

    public static FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }


    /**
     * convert m to km and round to 2 decimal places and return as string
     */
    public static String formatDistance(Double n) {
        if (n == null) {
            return null;
        }
        return Double.toString(Math.round((n / 1000) * 100.0) / 100.0);
    }

    /**
     * round number to 2 decimal places and return as string
     */
    public static String formatPace(Double n) {
        if (n == null) {
            return null;
        }
        return Double.toString(Math.round(n * 100.0) / 100.0);
    }

    /**
     * round number to integer
     */
    public static String formatCalories(Double n) {
        if (n == null) {
            return null;
        }
        return Long.toString(Math.round(n));
    }

    /* convert km to mi (multiply with a corresponding constant) */
    public static double kmToMi(Double n) {
        if (n == null) {
            return 0.0;
        }
        return n * Constants.KM_TO_MI;
    }

    /* convert m/s to mi/h (multiply with a corresponding constant) */
    public static double mpsToMiph(double n) {
        return n * Constants.MpS_TO_MIpH;
    }

    /* convert min/km to min/mi (multiply with a corresponding constant) */
    public static double minpkmToMinpmi(double n) {
        return n * Constants.MINpKM_TO_MINpMI;
    }

    /* ------------------------------------------------------------------------------------*/
    // TODO: PREVERI CE TE METODE DELUJEJO
    public static double convMillisecToSec(long n) {
        return (n / 1000.0);
    }

    public static double convMillisecToMin(long n) {
        return ((n / 1000.0) * (1.0 / 60.0));
    }

    public static double convMillisecToHour(long n) {
        return ((n / 1000.0) * (1 / 3600.0));
    }

    public static double convSecToMillisec(long n) {
        return n * 1000;

    }

    public static double mToKm(double distance) {
        return distance / 1000.0;
    }

    public static void showToastAndLog(Context context, String message) {
        Log.d("moj", message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
