package si.uni_lj.fri.pbd2019.runsup.pojo;

public class PersonAttribute {
    private String key;
    private Integer value;
    private String unit;

    public PersonAttribute() {
    }

    public PersonAttribute(String key, Integer value, String unit) {
        this.key = key;
        this.value = value;
        this.unit = unit;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
