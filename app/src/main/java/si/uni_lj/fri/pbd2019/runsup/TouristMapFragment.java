package si.uni_lj.fri.pbd2019.runsup;


import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.SupportMapFragment;

public class TouristMapFragment extends SupportMapFragment {

    private static final String TAG = "myTouristMapFragment";

    public static TouristMapFragment newInstance(int index) {

        TouristMapFragment f = new TouristMapFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        Log.d(TAG, " TouristMapFragment newInstance(int index)");

        return f;
    }

}
