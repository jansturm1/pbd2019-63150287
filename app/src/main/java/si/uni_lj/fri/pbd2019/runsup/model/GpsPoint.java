package si.uni_lj.fri.pbd2019.runsup.model;

import android.location.Location;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "gps_points")
public class GpsPoint {

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private Long id;

    // ce ne bo delal morda manjka 'foreignAutoCreate=true'
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "workout_id")
    private Workout workout;

    @DatabaseField
    private Long sessionNumber;

    @DatabaseField
    private Double latitude;

    @DatabaseField
    private Double longitude;

    @DatabaseField
    private Long duration;

    @DatabaseField
    private Float speed;

    @DatabaseField
    private Double pace;

    @DatabaseField
    private Double totalCalories;

    @DatabaseField
    private Date created;

    @DatabaseField
    private Date lastUpdate;

    public GpsPoint() {
    }

    public GpsPoint(Workout workout, Long sessionNumber, Location location, Long duration, Float speed, Double pace, Double totalCalories) {
        this.workout = workout;
        this.sessionNumber = sessionNumber;
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.duration = duration;
        this.speed = speed;
        this.pace = pace;
        this.totalCalories = totalCalories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    public Long getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(Long sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Double getPace() {
        return pace;
    }

    public void setPace(Double pace) {
        this.pace = pace;
    }

    public Double getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(Double totalCalories) {
        this.totalCalories = totalCalories;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "GpsPoint{" +
                "id=" + id +
                ", workout=" + workout +
                ", sessionNumber=" + sessionNumber +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", duration=" + duration +
                ", speed=" + speed +
                ", pace=" + pace +
                ", totalCalories=" + totalCalories +
                ", created=" + created +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
