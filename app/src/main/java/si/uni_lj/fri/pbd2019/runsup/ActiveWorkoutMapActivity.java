package si.uni_lj.fri.pbd2019.runsup;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import si.uni_lj.fri.pbd2019.runsup.helpers.MapsUtils;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;

public class ActiveWorkoutMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "ActiveWorkoutMap";
    private GoogleMap mMap;
    private Runnable mRunnable;
    private Handler mHandler;
    private SupportMapFragment mMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_activeworkoutmap_map);
        mMapFragment.getMapAsync(this);

        Button buttonBack = (Button) findViewById(R.id.button_activeworkoutmap_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initBroadCastLoop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady1");
        mMap = googleMap;
        mHandler.postDelayed(mRunnable, 0L);
        Log.d(TAG, "onMapReady2");
    }

    private void initBroadCastLoop() {
        mHandler = new Handler();
        mRunnable = new Runnable() {
            public void run() {
                drawMap();
                mHandler.postDelayed(this, Constants.DELAY_ACTIVE_MAP_REDRAW);
            }
        };
    }

    private void drawMap() {
        //Workout workout = null; // Preberi iz baze/intenta
        Log.d("MapsUtils", "drawMap()");

        DatabaseService databaseService = DatabaseService.getInstance(getApplicationContext());
        Workout workout = databaseService.findLastWorkout();
        if (workout != null) {
            MapsUtils.drawMap(getApplicationContext(), mMap, workout, false);
        }
    }
}
