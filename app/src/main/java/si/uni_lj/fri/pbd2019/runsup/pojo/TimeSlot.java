package si.uni_lj.fri.pbd2019.runsup.pojo;

public class TimeSlot {
    private long fromMillis = 0;
    private long toMillis = 0;

    public TimeSlot() {
    }

    public long getFromMillis() {
        return fromMillis;
    }

    public void setFromMillis(long fromMillis) {
        this.fromMillis = fromMillis;
    }

    public long getToMillis() {
        return toMillis;
    }

    public void setToMillis(long toMillis) {
        this.toMillis = toMillis;
    }

    public long getDurationMillis() {
        return this.getToMillis() - this.getFromMillis();
    }
}