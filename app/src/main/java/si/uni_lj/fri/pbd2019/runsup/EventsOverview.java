package si.uni_lj.fri.pbd2019.runsup;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.pojo.Event;
import si.uni_lj.fri.pbd2019.runsup.services.FirestoreService;

public class EventsOverview extends AppCompatActivity implements OnMapReadyCallback, OnMarkerClickListener, OnCompleteListener<QuerySnapshot>, SeekBar.OnSeekBarChangeListener {

    private static String TAG = "EventsOverview";

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private SeekBar seekBar;
    private Button buttonJoinWorkout;
    private Button buttonLeaveWorkout;
    private Button buttonEditWorkout;

    private List<Event> eventsList;
    private Polyline polyline;
    private List<Marker> markersList;
    private Marker lastMarkerClicked;
    private User user;
    private Circle circle;
    private LatLng latLngLjubljana;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_overview);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        eventsList = new ArrayList<>();
        markersList = new ArrayList<>();

        latLngLjubljana = new LatLng(46.056946, 14.505752);

        user = MainHelper.getLoggedInUser(getApplicationContext());
        Log.d(TAG, "loggedInUser= " + user);

        seekBar = findViewById(R.id.mapoverview_seekBar);
        seekBar.setOnSeekBarChangeListener(this);

        buttonEditWorkout = findViewById(R.id.eventsoverview_editworkout);
        buttonEditWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            }
        });

        buttonJoinWorkout = findViewById(R.id.eventsoverview_joinworkout);
        buttonJoinWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                for (Event event : eventsList) {
                    if (event.getStart().equals(lastMarkerClicked.getPosition())) {
                        event.addParticipant(user.getAccId()); // add logged user to event
                        FirestoreService.getInstance(getApplicationContext()).saveEvent(event, new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                Log.d(TAG, "DocumentSnapshot successfully updated!");
                                buttonJoinWorkout.setVisibility(View.GONE);
                                buttonLeaveWorkout.setVisibility(View.VISIBLE);
                            }
                        }, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error updating document", e);
                            }
                        });
                        break;
                    }
                }
            }
        });

        buttonLeaveWorkout = findViewById(R.id.eventsoverview_leaveworkout);
        buttonLeaveWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                for (Event event : eventsList) {
                    if (event.getStart().equals(lastMarkerClicked.getPosition())) {
                        event.removeParticipant(user.getAccId()); // remove logged user from event
                        FirestoreService.getInstance(getApplicationContext()).saveEvent(event, new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                Log.d(TAG, "DocumentSnapshot successfully updated!");
                                buttonJoinWorkout.setVisibility(View.VISIBLE);
                                buttonLeaveWorkout.setVisibility(View.GONE);
                            }
                        }, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error updating document", e);
                            }
                        });
                        break;
                    }
                }
            }
        });

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map_event_overview);
        mMapFragment.getMapAsync(this);
    }

    public boolean isInCircle(LatLng pos) {
        LatLng c = circle.getCenter();

        Location start = new Location("a");
        Location end = new Location("b");


        start.setLatitude(c.latitude);
        start.setLongitude(c.longitude);

        end.setLatitude(pos.latitude);
        end.setLongitude(pos.longitude);

        return start.distanceTo(end) < circle.getRadius();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        drawMarkerWithCircle(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        drawMarkers();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady");
        mMap = googleMap;
        //mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.google_maps_style));
        WorkoutInfoWindowAdapter workoutInfoWindow = new WorkoutInfoWindowAdapter(this);
        mMap.setInfoWindowAdapter(workoutInfoWindow);

        mMap.setOnMarkerClickListener(this);
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                circle.setCenter(mMap.getCameraPosition().target);
            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                drawMarkers();
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngLjubljana, 12));

        FirestoreService.getInstance(getApplicationContext()).getEvents(this);

        drawMarkerWithCircle(seekBar.getProgress());
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        for (DocumentSnapshot ds : task.getResult().getDocuments()) {
            Event event = Event.fromMap(ds);
            eventsList.add(event);
        }

        drawMarkers();
    }

    private void drawMarkers() {
        for (Marker m : markersList) {
            m.remove();
        }

        for (Event event : eventsList) {
            if (isInCircle(event.getStart())) {
                Marker marker = mMap.addMarker(new MarkerOptions().position(event.getStart()));
                markersList.add(marker);
            }
        }
    }

    @Override
    public boolean onMarkerClick(final Marker markerCurrent) {

        if (lastMarkerClicked != null) {
            LatLng position = lastMarkerClicked.getPosition();
            Object tag = lastMarkerClicked.getTag();
            lastMarkerClicked.remove();
            Marker marker = mMap.addMarker(new MarkerOptions().position(position));
            markersList.add(marker);
            markerCurrent.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        for (Event event : eventsList) {

            if (markerCurrent.getPosition().equals(event.getStart()) && user != null && event.getAuthorUid() != null) {

                lastMarkerClicked = markerCurrent;

                if (polyline != null) {
                    polyline.remove();
                }
                markerCurrent.setTag(event);
                markerCurrent.showInfoWindow();
                markerCurrent.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

                List<LatLng> line = PolyUtil.decode(event.getPolyline());
                polyline = mMap.addPolyline(new PolylineOptions()
                        .addAll(line)
                        .color(Color.BLACK));

                boolean userJoinedWorkout = false;

                for (String uid : event.getParticipantsUid()) {
                    if (uid.equals(user.getAccId())) {
                        Log.d("MOJ", "4");
                        userJoinedWorkout = true;
                    }
                }

                Log.d("MOJ", "===========================");
                Log.d("MOJ", "participants");
                for (String uid : event.getParticipantsUid()) {
                    Log.d("MOJ", "UID= " + uid);
                }
                Log.d("MOJ", "===========================");

                if (user.getAccId().equals(event.getAuthorUid())) {
                    buttonEditWorkout.setVisibility(View.VISIBLE);
                    buttonJoinWorkout.setVisibility(View.GONE);
                    buttonLeaveWorkout.setVisibility(View.GONE);
                } else if (userJoinedWorkout) {
                    buttonEditWorkout.setVisibility(View.GONE);
                    buttonJoinWorkout.setVisibility(View.GONE);
                    buttonLeaveWorkout.setVisibility(View.VISIBLE);
                } else {
                    buttonEditWorkout.setVisibility(View.GONE);
                    buttonJoinWorkout.setVisibility(View.VISIBLE);
                    buttonLeaveWorkout.setVisibility(View.GONE);

                }

            }
        }
        return true;
    }

    private void drawMarkerWithCircle(double radiusInMeters) {
        if (circle != null) {
            circle.setRadius(radiusInMeters);
        } else if (mMap != null) {
            int strokeColor = 0xffff0000;
            int shadeColor = 0x44ff0000;
            CircleOptions circleOptions = new CircleOptions().center(latLngLjubljana).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
            circle = mMap.addCircle(circleOptions);
        }
    }

}

