package si.uni_lj.fri.pbd2019.runsup.services;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class DatabaseService {

    private static final String TAG = "DatabaseService";
    private static DatabaseService databaseService = null;
    private static Dao<GpsPoint, Long> gpsDao;
    private static Dao<Workout, Long> workoutDao;
    private static Dao<User, Long> userDao;
    private static Dao<UserProfile, Long> userProfileDao;

    public static DatabaseService getInstance(Context context) {
        if (databaseService == null) {
            try {
                databaseService = new DatabaseService();
                DatabaseHelper dbHelper = new DatabaseHelper(context);
                gpsDao = dbHelper.gpsPointDao();
                workoutDao = dbHelper.workoutDao();
                userDao = dbHelper.userDao();
                userProfileDao = dbHelper.userProfileDao();
            } catch (Exception ex) {
                Log.e("DatabaseService", "Cannot initialize DatabaseService class");
            }
        }
        return databaseService;
    }

    public void insertGpsPoint(Workout workout, long sessionNumber, Location location, long duration,
                               List<Float> speedList, double pace, double totalCalories) throws SQLException {
        Float speed = (speedList != null && speedList.size() >= 1) ? speedList.get(speedList.size() - 1) : null; // last speed
        GpsPoint gpsPoint = new GpsPoint(workout, sessionNumber, location, duration, speed, pace, totalCalories);
        gpsDao.create(gpsPoint);
    }

    public List<GpsPoint> findAllGpsPoints() throws SQLException {
        List<GpsPoint> list = gpsDao.queryForAll();
        return list;
    }

    public GpsPoint findLastGpsPoint() {
        try {
            List<GpsPoint> gpsPointsList = gpsDao.queryForAll();
            return ((gpsPointsList != null && gpsPointsList.size() > 0) ? gpsPointsList.get(gpsPointsList.size() - 1)
                    : null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User findLastUser() {
        try {
            List<User> usersList = userDao.queryForAll();
            return ((usersList != null && usersList.size() > 0) ? usersList.get(usersList.size() - 1)
                    : null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Workout findWorkoutById(Long id) throws SQLException {
        return workoutDao.queryForId(id);
    }

    public void insertWorkout(Workout workout) throws SQLException {
        workoutDao.create(workout);
    }

    public void updateWorkout(Workout workout) throws SQLException {
        workoutDao.update(workout);
    }

    public Workout findLastWorkout() {
        try {
            List<Workout> workoutList = workoutDao.queryForAll();
            return ((workoutList != null && workoutList.size() > 0) ? workoutList.get(workoutList.size() - 1) : null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Workout> getAllWorkouts() {
        try {
            return workoutDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User findUserByAccId(String accId) {
        List<User> users = null;
        try {
            users = userDao.queryForEq("accId", accId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (users != null && users.size() > 0) ? users.get(0) : null;
    }

    public User findUserById(Long id) {
        User userInDb = null;
        try {
            userInDb = userDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userInDb;
    }

    public void createOrUpdateWorkout(Workout workout) {
        try {
            workoutDao.createOrUpdate(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertUser(User user) throws SQLException {
        User userInDb = null;
        userInDb = this.findUserByAccId(user.getAccId());

        if (userInDb == null) {
            userDao.create(user);
        }
    }

    public void deleteWorkoutById(Long id) throws SQLException {
        workoutDao.deleteById(id);
    }

    public void deleteAllWorkouts() throws SQLException {
        workoutDao.deleteBuilder().delete();
    }

    public void updateUser(User user) {
        try {
            User userInDb = this.findUserByAccId(user.getAccId());
            if (userInDb != null) {
                if (user.getAccId() != null) {
                    userInDb.setAccId(user.getAccId());
                }
                if (user.getAuthToken() != null) {
                    userInDb.setAuthToken(user.getAuthToken());
                }
                userDao.update(userInDb);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // testing

    public Workout createWorkout(int sportActivity, int status) {
        Workout workoutNew = new Workout();
        workoutNew.setSportActivity(sportActivity);
        workoutNew.setStatus(status);
        try {
            workoutDao.create(workoutNew);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "WorkoutId= " + workoutNew.getId());
        return workoutNew;
    }

    public void deleteAllWorkouts2() {
        try {
            workoutDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
