package si.uni_lj.fri.pbd2019.runsup;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MapsUtils;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // TODO: to ne bo delal ce bomo syncal zadnji nedokoncan workout: bo treba mau sprement
        DatabaseService databaseService = DatabaseService.getInstance(getApplicationContext());
        Workout workout = databaseService.findLastWorkout();
        if (workout != null) {
            MapsUtils.drawMap(getApplicationContext(), mMap, workout, true);
            mMap.moveCamera(getBounds(new ArrayList<GpsPoint>(workout.getGpsPoints())));
        }
    }

    private CameraUpdate getBounds(List<GpsPoint> gpsPoints) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        int padding = (int) (getResources().getDisplayMetrics().widthPixels * 0.35);
        for (GpsPoint gpsPoint : gpsPoints) {
            builder.include(new LatLng(gpsPoint.getLatitude(), gpsPoint.getLongitude()));
        }
        return CameraUpdateFactory.newLatLngBounds(builder.build(), padding);
    }

//    private CameraUpdate getBounds(List<Marker> markers) {
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        int padding = (int) (getResources().getDisplayMetrics().widthPixels * 0.35);
//        for (Marker marker : markers) {
//            builder.include(marker.getPosition());
//        }
//        return CameraUpdateFactory.newLatLngBounds(builder.build(), padding);
//    }
}
