package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.services.DatabaseService;
import si.uni_lj.fri.pbd2019.runsup.services.FirestoreService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class HistoryFragment extends android.support.v4.app.Fragment {

    private static final String TAG = "HistoryFragment";
    private ListView listView;
    private HistoryListAdapter mHistoryListAdapter;
    private DatabaseService databaseService;

    public HistoryFragment() {
    }

    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        databaseService = DatabaseService.getInstance(getContext());
    }

    @Override
    public void onResume() { // refresha history- potrebno po brisanju workouta, da imamo fresh seznam
        super.onResume();
        loadHistoryData();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem syncItem = menu.findItem(R.id.menu_history_action_sync_server);
        MenuItem clearHistoryItem = menu.findItem(R.id.menu_history_action_clear_history);
        if (MainHelper.isUserLoggedIn(getActivity().getApplicationContext())) {
            syncItem.setVisible(true);
        } else {
            syncItem.setVisible(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("History");  // set actionBar title
        return inflater.inflate(R.layout.fragment_history, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadHistoryData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.history, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_history_action_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                break;
            case R.id.menu_history_action_clear_history:
                new AlertDialog.Builder(getActivity())
                        .setMessage("Do you really want delete all workouts")
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteAllWorkouts();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                break;
            case R.id.menu_history_action_sync_server:
                User loggedInUser = MainHelper.getLoggedInUser(getActivity());
                FirestoreService.getInstance(getActivity().getApplicationContext()).syncWithServer(loggedInUser);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAllWorkouts() {
        DatabaseService databaseService = DatabaseService.getInstance(getContext());
        try {
            databaseService.deleteAllWorkouts();
            getFragmentManager().beginTransaction().detach(this).attach(this).commit(); // reload fragment after workouts delete
        } catch (SQLException e) {
            Toast.makeText(getActivity(), "Cannot delete workouts", Toast.LENGTH_SHORT);
            e.printStackTrace();
        }
    }

    private void loadHistoryData() {
        listView = (ListView) getActivity().findViewById(R.id.listview_history_workouts);
        TextView tvNoHistory = (TextView) getActivity().findViewById(R.id.textview_history_noHistoryData);
        ArrayList<Workout> workoutsList = new ArrayList<>(databaseService.getAllWorkouts());
        ArrayList<Workout> endedWorkoutsList = new ArrayList<>();

        User userLoggedIn = MainHelper.getLoggedInUser(getActivity().getApplicationContext());

        // ce je user prijavljen dodaj workoute na endedWorkoutsList
        if (userLoggedIn != null) {
            for (Workout w : workoutsList) {
                Log.d(TAG, "#uid= " + (w.getUser() != null ? w.getUser().getAccId() : "anonymous") + ", #w= " + w.getTitle());

                User workoutUser = w.getUser();

                // testi sicer ne grejo skoz.. v testu se workoutu ne nastavi userja !

                if (w.getStatus() != null) {
                    if (/**(workoutUser != null) && (workoutUser.getId() == userLoggedIn.getId()) &&**/(w.getStatus() == Workout.statusEnded)) {
                        endedWorkoutsList.add(w);
                    }
                }

            }

        }
        //  show history
        if (endedWorkoutsList.size() > 0 && userLoggedIn != null) {

            mHistoryListAdapter = new HistoryListAdapter(getContext(), endedWorkoutsList);
            listView.setAdapter(mHistoryListAdapter);
            final ArrayList<Workout> finalEndedWorkoutList = new ArrayList<>(endedWorkoutsList);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Workout workoutClicked = finalEndedWorkoutList.get(position);
                    getActivity().startActivity(getWorkoutDetailIntent(workoutClicked));
                }
            });

            listView.setVisibility(View.VISIBLE);
            tvNoHistory.setVisibility(View.INVISIBLE);

        } else {    // hide history
            listView.setVisibility(View.INVISIBLE);
            tvNoHistory.setVisibility(View.VISIBLE);
        }
    }

    private Intent getWorkoutDetailIntent(Workout workout) {
        Log.d(TAG, "workoutTitle= " + workout.getTitle());
        Intent intent = new Intent(getContext(), WorkoutDetailActivity.class);
        intent.putExtra("workoutId", workout.getId());
        return intent;
    }
}
