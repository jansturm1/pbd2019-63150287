package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class MapsUtils {

    private static String TAG = "MapsUtils";

    private static boolean isContinue(List<GpsPoint> gpsPoints, int index) {  // trenutni index zanima ce je nadaljevanje, sepravi gleda v preteklost
        if (index == 0) {   // je Start
            return false;
        }
        GpsPoint gpsPrev = gpsPoints.get(index - 1);
        GpsPoint gpsCurr = gpsPoints.get(index);
        return (gpsPrev.getSessionNumber() != gpsCurr.getSessionNumber());
    }

    private static boolean isPause(List<GpsPoint> gpsPoints, int index) { // trenutni index zanima ce bo pavza, sepravi gleda v prihodnost
        if ((index + 1) == gpsPoints.size()) {
            return false; // je End
        }
        GpsPoint gpsCurr = gpsPoints.get(index);
        GpsPoint gpsNext = gpsPoints.get(index + 1);
        return (gpsCurr.getSessionNumber() != gpsNext.getSessionNumber());
    }


    public static void drawMap(Context context, GoogleMap mMap, Workout workout, boolean everyThingVisible) {
        if (workout != null) {
            long curSess = -1;
            PolylineOptions options = null;
            LatLng lastPos = null;
            ArrayList<GpsPoint> gpsPoints = new ArrayList<>(workout.getGpsPoints());

            Marker breakMarker = null;

            int index = 0;


            // onStop => direkt prikaze break
            // si zapomnemo ta index (odPause)
            // ko pridemo na Continue: ce je distance < 100: izpišemo samo break na indexu od prej
            // sicer pa na indexu od prej izpiše Pause pa na trenutnem indexu Continue


            for (GpsPoint point : gpsPoints) {
                LatLng pointLL = new LatLng(point.getLatitude(), point.getLongitude());

                if (index == (gpsPoints.size() - 1)) {

                    if (workout.getStatus() == Workout.statusPaused) {
                        mMap.addMarker(new MarkerOptions()
                                .position(pointLL)
                                .title("Break= " + point.getSessionNumber()));
                    } else if (workout.getStatus() == Workout.statusEnded) {
                        mMap.addMarker(new MarkerOptions()
                                .position(pointLL)
                                .title("End"));
                    }
                    if (!everyThingVisible) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pointLL, 17));
                    }
                }

                if (index == 0 || isContinue(gpsPoints, index)) {

                    if (index == 0) {
                        mMap.addMarker(new MarkerOptions()
                                .position(pointLL)
                                .title("Start"));
                        options = new PolylineOptions().width(30).color(Color.BLUE);
                    } else {

                        float distance = 0;

                        if (lastPos != null) {
                            distance = MainHelper.getDistanceLastTwoLocations(latLngToLocation(lastPos), latLngToLocation(pointLL));
                        }

                        // ce je continue &&
                        // ce je razdalja od prejne pavze < 100 pol ne resetiramo options
                        if (distance < 100) {
                            // ne resetirej poti- naj ostane v enem kosu
                            breakMarker.setTitle(("Break " + point.getSessionNumber()));
                        } else {
                            // reset poti
                            mMap.addMarker(new MarkerOptions()
                                    .position(pointLL)
                                    .title(("Continue " + point.getSessionNumber())));
                            options = new PolylineOptions().width(30).color(Color.BLUE);
                        }
                    }
                } else if (isPause(gpsPoints, index)) {
                    Log.d(TAG, "isPause(), index= " + index);
                    breakMarker = mMap.addMarker(new MarkerOptions()
                            .position(pointLL)
                            .title("Pause " + point.getSessionNumber()));

                } else if ((index == 0) && (workout.getStatus() == 1)) {
                    mMap.addMarker(new MarkerOptions()
                            .position(pointLL)
                            .title("End"));
                }
                options.add(pointLL);

                if (lastPos != null) {
                    mMap.addPolyline(options);
                }

                lastPos = pointLL;
                index++;
            }
//            if (everyThingVisible && gpsPoints != null && gpsPoints.size() > 0) {
//                CameraUpdate cameraUpdate = getBounds(context, gpsPoints);
//                if (cameraUpdate != null) {
//                    mMap.moveCamera(cameraUpdate);
//                }
//            }
        }
    }

    private static Location latLngToLocation(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

//    private CameraUpdate getBounds(List<Marker> markers) {
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        int padding = (int) WorkoutDetailActivity.getContext().getResources().getDisplayMetrics().widthPixels * 0.35)
//        ;
//        for (Marker marker : markers) {
//            builder.include(marker.getPosition());
//        }
//        return CameraUpdateFactory.newLatLngBounds(builder.build(), padding);
//    }
}

