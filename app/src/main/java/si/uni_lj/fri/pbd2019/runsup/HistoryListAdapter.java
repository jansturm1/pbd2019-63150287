package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HistoryListAdapter extends ArrayAdapter<Workout> {

    private Context mContext;
    private List<Workout> list;

    public HistoryListAdapter(@NonNull Context context, @LayoutRes ArrayList<Workout> list) {
        super(context, 0, list);
        this.mContext = context;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.adapter_history, parent, false);

        Workout workout = this.list.get(position);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
        int distanceUnitInt = Integer.parseInt(sharedPref.getString("unit", "0"));   // zna biti problem dokler user ne klikne v settingsih zato nastavljen na 0

        ImageView icon = (ImageView) listItem.findViewById(R.id.imageview_history_icon);

        TextView tvTitle = (TextView) listItem.findViewById(R.id.textview_history_title);
        tvTitle.setText(workout.getTitle());

        TextView tvDateTime = (TextView) listItem.findViewById(R.id.textview_history_datetime);
        tvDateTime.setText(workout.toStringWorkoutTime());

        TextView tvSportActivity = (TextView) listItem.findViewById(R.id.textview_history_sportactivity);
        tvSportActivity.setText(workout.toStringWorkoutStatistics(getContext(), distanceUnitInt));

        return listItem;
    }
}
