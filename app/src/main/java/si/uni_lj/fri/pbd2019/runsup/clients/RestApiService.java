package si.uni_lj.fri.pbd2019.runsup.clients;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;
import si.uni_lj.fri.pbd2019.runsup.model.User;

public interface RestApiService {

    @GET
    Call<User> getUsers(@Url String url);
}
