package si.uni_lj.fri.pbd2019.runsup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.pojo.PersonAttribute;

public class LoginListAdapter extends ArrayAdapter<PersonAttribute> {

    private Context mContext;
    private List<PersonAttribute> list;

    public LoginListAdapter(@NonNull Context context, @LayoutRes ArrayList<PersonAttribute> list) {
        super(context, 0, list);
        this.mContext = context;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.adapter_login, parent, false);

        final PersonAttribute personAttribute = this.list.get(position);

        listItem.findViewById(R.id.adapter_login_linearlayout).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertDialog(personAttribute);
            }
        });

        TextView tvValue = (TextView) listItem.findViewById(R.id.adapter_login_key);
        tvValue.setText(personAttribute.getKey());

        TextView tvKey = (TextView) listItem.findViewById(R.id.adapter_login_value);
        tvKey.setText(Integer.toString(personAttribute.getValue()));

        TextView tvValueUnit = (TextView) listItem.findViewById(R.id.adapter_login_value_unit);
        tvValueUnit.setText(personAttribute.getUnit());

        return listItem;
    }

    private void alertDialog(final PersonAttribute personAttribute) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(personAttribute.getKey());
        final EditText editText = new EditText(getContext());
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setText(Integer.toString(personAttribute.getValue()));
        builder.setView(editText);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Integer value = Integer.parseInt(editText.getText().toString());
                    personAttribute.setValue(value);
                } catch (Exception ex) {
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
