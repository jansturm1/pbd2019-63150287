package si.uni_lj.fri.pbd2019.runsup.pojo;

import com.google.gson.annotations.SerializedName;

public class OverViewPolyline {
    private String points;
    private String warnings;
    @SerializedName("waypoint_order")
    private String waypointOrder;


    public OverViewPolyline() {
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public String getWaypointOrder() {
        return waypointOrder;
    }

    public void setWaypointOrder(String waypointOrder) {
        this.waypointOrder = waypointOrder;
    }

    @Override
    public String toString() {
        return "OverViewPolyline{" +
                "points='" + points + '\'' +
                ", warnings='" + warnings + '\'' +
                ", waypointOrder='" + waypointOrder + '\'' +
                '}';
    }
}