package si.uni_lj.fri.pbd2019.runsup.pojo;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Event {

    private Date dateCreated;   // primarni kljuc
    private Date dateStart;   // primarni kljuc
    private String authorUid;
    private String polyline;
    private LatLng start;
    private LatLng end;
    private String title;
    private Long distance;
    private Set<String> participantsUid;

    public Event() {
        participantsUid = new HashSet<>();
    }

    public Event(Date dateCreated, Date dateStart, String authorUid, String title, String polyline, LatLng start, LatLng end) {
        this.dateCreated = dateCreated;
        this.dateStart = dateStart;
        this.authorUid = authorUid;
        this.title = title;
        this.polyline = polyline;
        this.start = start;
        this.end = end;
        this.participantsUid = new HashSet<>();
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Set<String> getParticipantsUid() {
        return participantsUid;
    }

    public void setParticipantsUid(Set<String> participantsUid) {
        this.participantsUid = participantsUid;
    }

    public String getAuthorUid() {
        return authorUid;
    }

    public void setAuthorUid(String authorUid) {
        this.authorUid = authorUid;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public LatLng getStart() {
        return start;
    }

    public void setStart(LatLng start) {
        this.start = start;
    }

    public LatLng getEnd() {
        return end;
    }

    public void setEnd(LatLng end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public void addParticipant(String participantUid) {
        Log.d("Event.class", "addding participant= " + participantUid);
        participantsUid.add(participantUid);
        Log.d("Event.class", "participants= " + participantsUid);
    }

    public void removeParticipant(String participantUid) {
        participantsUid.remove(participantUid);
    }

    public Map toMap() {
        Map<String, Object> map = new HashMap<>();

        Map<String, String> participantsMap = new HashMap<>();
        for (String s : participantsUid) {
            participantsMap.put(s, s);
        }

        map.put("dateCreated", dateCreated.getTime());
        map.put("distance", distance);
        map.put("dateStart", dateStart.getTime());
        map.put("authorUid", authorUid);
        map.put("title", title);
        map.put("polyline", polyline);
        map.put("start", new GeoPoint(start.latitude, start.longitude));
        map.put("end", new GeoPoint(end.latitude, end.longitude));
        map.put("participantsUid", participantsMap);
        return map;
    }

    public static Event fromMap(DocumentSnapshot documentSnapshot) {

        if (documentSnapshot == null || documentSnapshot.getLong("dateCreated") == null) {
            return null;
        }

        Event e = new Event();
        GeoPoint gpStart = documentSnapshot.getGeoPoint("start");
        GeoPoint gpEnd = documentSnapshot.getGeoPoint("end");

        Map<String, Object> participantsMap = (Map<String, Object>) documentSnapshot.get("participantsUid");

        Set participantsSet = new HashSet<>();

        Log.d("Event.class", "adding participants...");

        for (String uid : participantsMap.keySet()) {
            Log.d("Event.class", "participant= " + uid);
            participantsSet.add(uid);
        }

        e.setDateCreated(new Date(documentSnapshot.getLong("dateCreated")));
        e.setDateStart(new Date(documentSnapshot.getLong("dateStart")));
        e.setDistance(documentSnapshot.getLong("distance"));
        e.setAuthorUid(documentSnapshot.getString("authorUid"));
        e.setTitle(documentSnapshot.getString("title").split("\\s+")[0]);
        e.setPolyline(documentSnapshot.getString("polyline"));
        e.setStart(new LatLng(gpStart.getLatitude(), gpStart.getLongitude()));
        e.setEnd(new LatLng(gpEnd.getLatitude(), gpEnd.getLongitude()));
        e.setParticipantsUid(participantsSet);

        return e;
    }

    @Override
    public String toString() {
        return "Event{" +
                "dateCreated=" + dateCreated +
                ", dateStart=" + dateStart +
                ", authorUid='" + authorUid + '\'' +
                ", polyline='" + polyline + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", title='" + title + '\'' +
                ", distance=" + distance +
                ", participantsUid=" + participantsUid +
                '}';
    }
}
