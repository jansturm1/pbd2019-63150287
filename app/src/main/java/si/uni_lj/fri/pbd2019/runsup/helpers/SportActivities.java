package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import si.uni_lj.fri.pbd2019.runsup.Constants;

public final class SportActivities {

    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;


    public SportActivities() {
    }

    /**
     * Returns MET value for an activity.
     *
     * @param activityType - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param speed        - speed in m/s
     * @return
     */
    public static double getMET(int activityType, Float speed) {
        double speedMphDouble = MainHelper.mpsToMiph(speed);
        int speedMph = (int) Math.ceil(speedMphDouble);

        if (activityType == SportActivities.RUNNING) {
            return mapRunning.get(speedMph) != null ? mapRunning.get(speedMph) : speedMph * Constants.MET_AVG_RUNNING;
        } else if (activityType == SportActivities.WALKING) {
            return mapWalking.get(speedMph) != null ? mapWalking.get(speedMph) : speedMph * Constants.MET_AVG_WALKING;
        } else if (activityType == SportActivities.CYCLING) {
            return mapCycling.get(speedMph) != null ? mapCycling.get(speedMph) : speedMph * Constants.MET_AVG_CYCLING;
        }
        return 0;
    }

    /**
     * Returns final calories computed from the data provided (returns value in kcal)
     *
     * @param sportActivity               - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param weight                      - weight in kg
     * @param speedList                   - list of all speed values recorded (unit = m/s)
     * @param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
     * @return
     */


    // mislm da je to fora za timeFillingSpeedListInHours:
    // štoparca ves čas laufa in ta čas ni enak času med dvema speedlistoma, ker ni nujno da je bil
    // ta speed list sploh kdaj dodan na seznam tko, da se čas verjetno dost spreminja.
    /*  =========  TODO   ======================       */
    // METs × bodyMassKg × timePerformingHours
    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours) {

        if (speedList.size() < 1) {
            return 0;
        }
        double sumSpeed = 0;

        for (Float speed : speedList) {
            sumSpeed += speed;
        }
        double avgSpeed = (sumSpeed * 1.0) / speedList.size();
        double met = getMET(sportActivity, (float) avgSpeed);
        double calories = met * weight * timeFillingSpeedListInHours;
        return calories;
    }

    /**
     * Returns pace (NI V USER STORY)
     *
     * @param distance         - distance in meters
     * @param durationMillisec - duration in milliseconds
     * @return
     */

    public static double calculatePace(double distance, long durationMillisec) {
        if (distance <= 0 || durationMillisec <= 0) {
            return 0.0;
        }
        double durationMin = MainHelper.convMillisecToMin(durationMillisec);
        double distanceKm = MainHelper.mToKm(distance);
        return ((durationMin * 1.0) / distanceKm);
    }

    /**
     * Returns pace (NI V USER STORY)
     *
     * @param distance         - distance in meters
     * @param durationMillisec - duration in milliseconds
     * @return
     */

    public static float calculateSpeed(double distance, long durationMillisec) {
        Log.d("moj", "calculateSpeed: distance= " + distance + ", durationMillisec= " + durationMillisec);
        double durationSec = MainHelper.convMillisecToSec(durationMillisec);
        return (float) (distance / durationSec);
    }

    private static final Map<Integer, Double> mapRunning = new HashMap<Integer, Double>() {
        {
            put(4, 6.0);
            put(5, 8.3);
            put(6, 9.8);
            put(7, 11.0);
            put(8, 11.8);
            put(9, 12.8);
            put(10, 14.5);
            put(11, 16.0);
            put(12, 19.0);
            put(13, 19.8);
            put(14, 23.0);
        }

        ;
    };

    private static final Map<Integer, Double> mapWalking = new HashMap<Integer, Double>() {
        {
            put(1, 2.0);
            put(2, 2.8);
            put(3, 3.1);
            put(4, 3.5);
        }

        ;
    };

    private static final Map<Integer, Double> mapCycling = new HashMap<Integer, Double>() {
        {
            put(10, 6.8);
            put(12, 8.0);
            put(14, 10.0);
            put(16, 12.8);
            put(18, 13.6);
            put(20, 15.8);
        }

        ;
    };


}
