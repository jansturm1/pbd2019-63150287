package si.uni_lj.fri.pbd2019.runsup.pojo;

public class Leg {

    private Distance distance;

    public Leg() {
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    class Distance {
        private String text;
        private Long value;

        public Distance() {
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Long getValue() {
            return value;
        }

        public void setValue(Long value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Distance{" +
                    "text='" + text + '\'' +
                    ", value=" + value +
                    '}';
        }
    }
}
