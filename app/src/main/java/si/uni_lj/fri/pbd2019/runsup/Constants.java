package si.uni_lj.fri.pbd2019.runsup;

public class Constants {

    public static final String PACKAGE_NAME = "si.uni_lj.fri.pbd2019.runsup";

    public static final String COMMAND_START = PACKAGE_NAME + ".COMMAND_START";
    public static final String COMMAND_CONTINUE = PACKAGE_NAME + ".COMMAND_CONTINUE";
    public static final String COMMAND_PAUSE = PACKAGE_NAME + ".COMMAND_PAUSE";
    public static final String COMMAND_STOP = PACKAGE_NAME + ".COMMAND_STOP";
    public static final String UPDATE_SPORT_ACTIVITY = PACKAGE_NAME + ".UPDATE_SPORT_ACTIVITY";
    public static final String TICK = PACKAGE_NAME + ".TICK";


    // TrackerService
    public static final long DELAY_CLIENT = 1000;
    public static final long DELAY_ACTIVE_MAP_REDRAW = 1000L * 15; // na 15 sekund redrawamo mapo

    public static final int WEIGHT = 60;

    public static final double MET_AVG_RUNNING = 1.535353535;
    public static final double MET_AVG_WALKING = 1.14;
    public static final double MET_AVG_CYCLING = 0.744444444;

    public static final float MpS_TO_MIpH = 2.23694f;
    public static final float KM_TO_MI = 0.62137119223734f;
    public static final float MINpKM_TO_MINpMI = 1.609344f;

    /*  TODO:    CONSTANTS- TREBA PREGLED KER TREUNTNO SO VREDNOSTI ZA TESITRANJE IN NE PRAVE      */
    public static final long LOCATION_SMALLEST_DISPLACEMENT = 10;     // more bit 10m !!!!
    public static final long LOCATION_REQUEST_INTERVAL = 3000;       // 3000 msec
    public static final long CONTINUE_MAX_DISTANCE = 100;
    /*      -----------------------------------------------------------------------------      */

}
