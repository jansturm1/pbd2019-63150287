package si.uni_lj.fri.pbd2019.runsup.settings;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import javax.annotation.Nullable;

import si.uni_lj.fri.pbd2019.runsup.R;


public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = "SettingsActivity";
    private static boolean isGpsChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // back button

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PrefsFragment()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class PrefsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
//            CheckBox checkBox = (CheckBox) getActivity().findViewById(R.id.checkbox_gps);
//            checkBox.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    isGpsChecked = ((CheckBox) v).isChecked();
//                    Log.d(TAG, "isGPSChecked= " + isGpsChecked);
//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString(getString(R.string.shared_dsd), "da2");
//                    editor.putBoolean(getString(R.string.shared_jwbe), isGpsChecked);
//                    editor.commit();
//                }
//            });

            ListPreference listPreference = (ListPreference) findPreference(getString(R.string.pref_main_key));
            if (listPreference.getValue() == null) {
                listPreference.setValueIndex(0);
            }
            listPreference.setSummary(listPreference.getEntry());
            listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    preference.setSummary(newValue.toString());
                    return true;
                }
            });

        }
    }
}