package si.uni_lj.fri.pbd2019.runsup.services;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import si.uni_lj.fri.pbd2019.runsup.R;

public class DirectionsService {

    public static String TAG = "DirectionsService";
    private static DirectionsService directionsService;
    private static Context context;
    private static List<LatLng> potionsList;

    public static DirectionsService getInstance(Context contx) {
        if (directionsService == null) {
            directionsService = new DirectionsService();
            context = contx;
            potionsList = new ArrayList<>();
        }
        return directionsService;
    }

    public List<LatLng> getPotionsList() {
        return potionsList;
    }

    public int getPositionListSize() {
        return (potionsList != null ? potionsList.size() : 0);
    }

    public void addPosition(LatLng latLng) {
        if (potionsList != null) {
            potionsList.add(latLng);
        }
    }

    public void removeAllPositions() {
        if (potionsList != null) {
            potionsList.clear();
        }
    }

    public void fetchPolyline(GoogleMap map, Callback callback) {
        String url = getDirectionsUrl();
        if (url != null) {
            getLineAsync(map, url, callback);
        } else {
            Log.d(TAG, "url is null (postionList==null || postionsList.size() < 2)");
        }
    }

    private String getDirectionsUrl() {

        if (potionsList == null || potionsList.size() < 2) {
            return null;
        }
        String originStr = "origin=" + potionsList.get(0).latitude + "," + potionsList.get(0).longitude;
        String destStr = "destination=" + potionsList.get(potionsList.size() - 1).latitude + "," + potionsList.get(potionsList.size() - 1).longitude;
        String keyStr = "key= " + context.getResources().getString(R.string.google_api_key);
        String modeStr = "mode=walking";

        String waypoints = "waypoints=";
        for (int i = 2; i < potionsList.size(); i++) {
            waypoints += potionsList.get(i).latitude + "," + potionsList.get(i).longitude + "|";
        }
        String parameters = originStr + "&" + destStr + "&" + waypoints + "&" + modeStr + "&" + keyStr;
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + parameters;
        Log.d(TAG, "url= " + url);
        return url;

    }


    private void getLineAsync(GoogleMap map, String url, Callback callback) {

        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        httpClient.newCall(request).enqueue(callback);
    }


}
